# Souk change log

## [v2.0.0] (Release date: 10.10.2022)

First LKEC-200 release.
Fork from oc 4.0.1.1.
Installer, languages and database.

## [v1.0.0] (Release date: 10.10.2022)

First LKEC-100 release.
Fork from oc 3.0.3.8.
Added conodr patches.
installer, languages and database.
