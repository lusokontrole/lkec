<?php
$_['heading_title']		= 'Redefinir palavra-passe';
$_['text_password']		= 'Indique a nova palavra-passe a utilizar.';
$_['text_success']		= 'A sua palavra-passe foi redefinida.';
$_['entry_password']		= 'Nova palavra-passe:';
$_['entry_confirm']		= 'Confirmar palavra-passe:';
$_['error_password']		= 'A nova palavra-passe deve ter entre 5 e 20 caracteres!';
$_['error_confirm']		= 'A palavra-passe de confirmação está errada!';
