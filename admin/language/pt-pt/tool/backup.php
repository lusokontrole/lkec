<?php
$_['heading_title']		= 'Backup/Restore';
$_['text_success']		= 'Base de dados importada com sucesso!';
$_['entry_progress']		= 'Progress';
$_['entry_export']		= 'Exportar';
$_['tab_backup']		= 'Backup';
$_['tab_restore']		= 'Restore';
$_['error_permission']		= 'Atenção: Não tem permissão para fazer Backup/Restore!';
$_['error_export']		= 'Warning: You must select at least one table to export!';
$_['error_file']		= 'File could not be found!';
