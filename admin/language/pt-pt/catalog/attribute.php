<?php
$_['heading_title']		= 'Atributos';
$_['text_success']		= 'Atributo modificado com sucesso!';
$_['text_list']		= 'Lista de Atributos';
$_['text_add']		= 'Novo atributo';
$_['text_edit']		= 'Alterar atributo';
$_['column_name']		= 'Nome do Atributo';
$_['column_attribute_group']		= 'Grupo de Atributos';
$_['column_sort_order']		= 'Ordem';
$_['column_action']		= 'Ação';
$_['entry_name']		= 'Nome do atributo';
$_['entry_attribute_group']		= 'Grupo de atributos';
$_['entry_sort_order']		= 'Ordem';
$_['error_warning']		= 'Warning: Please check the form carefully for errors!';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar os atributos!';
$_['error_attribute_group']		= 'Grupo de Atributo Obrigatório!';
$_['error_name']		= 'O atributo deve ter entre 3 e 64 caracteres!';
$_['error_product']		= 'Atenção: Este atributo não pode ser removido, pois está associado a %s produtos!';
