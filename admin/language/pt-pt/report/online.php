<?php
$_['heading_title']		= 'Relatório Online';
$_['text_extension']		= 'Extensões';
$_['text_success']		= 'Relatório online dos clientes modificado com sucesso!';
$_['text_list']		= 'Lista Online';
$_['text_filter']		= 'Filtro';
$_['text_guest']		= 'Convidado';
$_['column_ip']		= 'IP';
$_['column_customer']		= 'Cliente';
$_['column_url']		= 'Última página visitada';
$_['column_referer']		= 'Loja';
$_['column_date_added']		= 'Último Click';
$_['column_action']		= 'Ação';
$_['entry_ip']		= 'IP';
$_['entry_customer']		= 'Cliente';
$_['entry_status']		= 'Estado';
$_['entry_sort_order']		= 'Ordem de classificação';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar o relatório on-line dos clientes!';
