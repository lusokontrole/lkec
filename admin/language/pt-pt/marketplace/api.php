<?php
$_['heading_title']		= 'Souk Marketplace API';
$_['text_success']		= 'Modificou a sua informação da API com sucesso!';
$_['text_signup']		= 'Digite suas informações da Souk API para obter <a href="https://khadija.agency/index.php?route=account/store" target="_blank" class="alert-link">aqui</a>.';
$_['entry_username']		= 'Username';
$_['entry_secret']		= 'Secret';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar a API!';
$_['error_username']		= 'Username obrigatório!';
$_['error_secret']		= 'Secret obrigatório!';
