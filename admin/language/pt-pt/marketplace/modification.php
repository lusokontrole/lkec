<?php
$_['heading_title']		= 'Modificações';
$_['text_success']		= 'Modificou as Modificações com sucesso!';
$_['text_refresh']		= 'Sempre que  activar / desativar ou eliminar uma modificação, precisa clicar no botão atualizar para criar a cache de modificação!';
$_['text_list']		= 'Lista de Modificações';
$_['column_name']		= 'Nome da Modificação';
$_['column_author']		= 'Autor';
$_['column_version']		= 'Versão';
$_['column_status']		= 'Estado';
$_['column_date_added']		= 'Data Adicionado';
$_['column_action']		= 'Ação';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar as modificações!';
