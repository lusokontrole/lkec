<?php
$_['heading_title']		= 'Expedição por peso';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Expedição por peso modificado com sucesso!';
$_['text_edit']		= 'Configurações da expedição por peso';
$_['entry_rate']		= 'Valor por peso';
$_['entry_tax_class']		= 'Classe de impostos';
$_['entry_geo_zone']		= 'Região geográfica';
$_['entry_status']		= 'Estado';
$_['entry_sort_order']		= 'Posição';
$_['help_rate']		= 'Exemplo: 5:10.00,7:12.00 Peso:Valor,Peso:Valor, etc...';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar da expedição por peso!';
