<?php
$_['heading_title']		= 'Estatísticas de Produtos Visualizados';
$_['text_extension']		= 'Extensions';
$_['text_edit']		= 'Edit Products Viewed Report';
$_['text_success']		= 'Visualizações Actualizadas!';
$_['column_name']		= 'Nome do Produto';
$_['column_model']		= 'Modelo';
$_['column_viewed']		= 'Visualizações';
$_['column_percent']		= 'Percentagem';
$_['entry_status']		= 'Status';
$_['entry_sort_order']		= 'Sort Order';
$_['error_permission']		= 'Atenção: Você não tem permissão para acessar o Estatísticas de produtos visualizados!';
