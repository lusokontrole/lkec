<?php
$_['heading_title']		= 'Novos Produtos';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Módulo de novos produtos modificado com sucesso!';
$_['text_edit']		= 'Configurações do módulo de novos produtos';
$_['entry_name']		= 'Módulo';
$_['entry_limit']		= 'Limite';
$_['entry_width']		= 'Largura';
$_['entry_height']		= 'Altura';
$_['entry_status']		= 'Estado';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar o módulo novos produtos!';
$_['error_name']		= 'O módulo deve ter entre 3 e 64 caracteres!';
$_['error_width']		= 'A largura é obrigatória!';
$_['error_height']		= 'A altura é obrigatória!';
