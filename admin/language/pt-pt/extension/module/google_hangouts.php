<?php
$_['heading_title']		= 'Google Hangouts';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Módulo Google Hangouts modificado com sucesso!';
$_['text_edit']		= 'Configurações do módulo Google Hangouts';
$_['entry_code']		= 'Código de integração';
$_['entry_status']		= 'Estado';
$_['help_code']		= '<a href="https://developers.google.com/+/hangouts/button" target="_blank">Registe uma conta no Google Hangout</a>, gere o código de integração, copie e cole o código de integração no campo.';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar o módulo Google Hangouts!';
$_['error_code']		= 'Código de integração';
