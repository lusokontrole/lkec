<?php
$_['heading_title']		= 'Currency Rates';
$_['text_success']		= 'Success: You have modified currencies rates!';
$_['column_name']		= 'Currency Rate Name';
$_['column_status']		= 'Status';
$_['column_action']		= 'Action';
$_['error_permission']		= 'Warning: You do not have permission to modify currencies rates!';
