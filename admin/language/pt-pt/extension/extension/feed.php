<?php
$_['heading_title']		= 'Alimentadores';
$_['text_success']		= 'Alimentador modificado com sucesso!';
$_['column_name']		= 'Alimentador';
$_['column_status']		= 'Situação';
$_['column_action']		= 'Ação';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar alimentadores!';
