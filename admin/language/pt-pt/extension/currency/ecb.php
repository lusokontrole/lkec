<?php
$_['heading_title']		= 'European Central Bank Currency Converter';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Success: You have modified fixer ECB rates!';
$_['text_edit']		= 'Edit European Central Bank';
$_['text_support']		= 'This extension requires at EUR currency to be available currency option.';
$_['entry_status']		= 'Status';
$_['error_permission']		= 'Warning: You do not have permission to modify ECB currency rates!';
