<?php
$_['heading_title']		= 'Sub-total';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Sub-total modificado com sucesso!';
$_['text_edit']		= 'Configurações do Sub-total';
$_['entry_status']		= 'Estado';
$_['entry_sort_order']		= 'Posição';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar o Sub-total!';
