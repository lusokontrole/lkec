<?php
$_['heading_title']		= 'Pontos';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Pontos modificado com sucesso!';
$_['text_edit']		= 'Configurações dos pontos';
$_['entry_status']		= 'Estado';
$_['entry_sort_order']		= 'Posição';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar os Pontos!';
