<?php
$_['heading_title']		= 'Soluções de Devoluções';
$_['text_success']		= 'Solução de devoluções modificada com sucesso!';
$_['text_list']		= 'Lista de Soluções de Devoluções';
$_['text_add']		= 'Nova solução de devolução';
$_['text_edit']		= 'Alterar solução de devolução';
$_['column_name']		= 'Solução de Devolução';
$_['column_action']		= 'Ação';
$_['entry_name']		= 'Solução de devolução';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar solução de devoluções!';
$_['error_name']		= 'A solução de devoluções deve ter entre 3 e 64 caracteres!';
$_['error_return']		= 'Atenção: Esta solução de devoluções não pode ser removida, pois está associada a %s produtos devolvidos!';
