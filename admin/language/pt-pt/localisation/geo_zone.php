<?php
$_['heading_title']		= 'Regiões Geográficas';
$_['text_success']		= 'Região geográfica modificada com sucesso!';
$_['text_list']		= 'Lista de Regiões Geográficas';
$_['text_add']		= 'Nova região geográfica';
$_['text_edit']		= 'Alterar região geográfica';
$_['text_geo_zone']		= 'Geo Zones';
$_['column_name']		= 'Região Geográfica';
$_['column_description']		= 'Descrição';
$_['column_action']		= 'Ação';
$_['entry_name']		= 'Região geográfica';
$_['entry_description']		= 'Descrição';
$_['entry_country']		= 'País';
$_['entry_zone']		= 'Distrito';
$_['error_permission']		= 'Atenção: Não possui permissão para modificar as regiões geográficas!';
$_['error_name']		= 'A região geográfica deve ter entre 3 e 32 caracteres!';
$_['error_description']		= 'A descrição deve ter entre 3 e 255 caracteres!';
$_['error_tax_rate']		= 'Atenção: Esta região geográfica não pode ser removida, pois está assciada a um ou mais taxas de impostos!';
