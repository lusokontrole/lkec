<?php
$_['text_subject']		= '%s - A sua conta de afiliado foi ativada!';
$_['text_welcome']		= 'Bem-vindo e obrigado por se registrar em %s!';
$_['text_login']		= 'A sua conta já foi aprovada e pode fazer login usando seu endereço de e-mail e senha visitando o nosso site ou no seguinte URL:';
$_['text_service']		= 'Upon logging in, you will be able to generate tracking codes, track commission payments and edit your account information.';
$_['text_thanks']		= 'Obrigado';
$_['button_login']		= 'Login';
