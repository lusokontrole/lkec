<?php
$_['heading_title']		= 'APIs';
$_['text_success']		= 'Acesso à API modificado com sucesso!';
$_['text_list']		= 'Lista de Utilizadores da API';
$_['text_add']		= 'Novo utilizador da API';
$_['text_edit']		= 'Alterar utilizador da API';
$_['text_ip']		= 'Em baixo pode criar uma lista de IP\'s com permissão para aceder à API. O seu IP é %s';
$_['column_username']		= 'Utilizador';
$_['column_status']		= 'Estado';
$_['column_token']		= 'Token';
$_['column_ip']		= 'IP';
$_['column_date_added']		= 'Adicionado';
$_['column_date_modified']		= 'Modificado';
$_['column_action']		= 'Ação';
$_['entry_username']		= 'Utilizador';
$_['entry_key']		= 'API Key';
$_['entry_status']		= 'Estado';
$_['entry_ip']		= 'IP';
$_['error_permission']		= 'Atenção: Não tem premissões para modificar APIs!';
$_['error_username']		= 'O utilizador deve ter entre 3 e 20 caracteres!';
$_['error_key']		= 'API Key tem que ter entre 64 e 256 caracteres!';
$_['error_ip']		= 'You must have at least one IP added to the allowed list!';
