<?php
$_['text_subject']		= '¡%s - Tu cuenta de afiliado ha sido activada!';
$_['text_welcome']		= '¡Bienvenido y gracias por registrarte en %s!';
$_['text_login']		= 'Su cuenta ahora ha sido aprobada y puede iniciar sesión utilizando su dirección de correo electrónico y contraseña visitando nuestro sitio web o en la siguiente URL:';
$_['text_service']		= 'Upon logging in, you will be able to generate tracking codes, track commission payments and edit your account information.';
$_['text_thanks']		= 'Gracias,';
$_['button_login']		= 'Login';
