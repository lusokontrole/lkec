<?php
$_['heading_title']		= 'Respaldar y Restaurar';
$_['text_success']		= '¡Usted ha importado exitósamente la base de datos!';
$_['entry_progress']		= 'Progreso';
$_['entry_export']		= 'Exportar';
$_['tab_backup']		= 'Respaldar';
$_['tab_restore']		= 'Restaurar';
$_['error_permission']		= 'Advertencia: ¡No tiene permisos para modificar el Respaldo ni para Restaurar!';
$_['error_export']		= 'Advertencia: ¡Debe seleccionar al menos una tabla para exportar!';
$_['error_file']		= '¡El archivo no se encontró!';
