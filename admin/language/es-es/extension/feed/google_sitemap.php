<?php
$_['heading_title']		= 'Google Sitemap';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat els Feeds de Google Sitemap!';
$_['text_edit']		= 'Edit Google Sitemap';
$_['entry_status']		= 'Estat:';
$_['entry_data_feed']		= 'Dades de l&#039;Url del Feed:';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per a modificar Feeds de Google Sitemap!';
