<?php
$_['heading_title']		= 'Transport';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat el Total del Transport!';
$_['text_edit']		= 'Edit Shipping Total';
$_['entry_estimator']		= 'Estimaci&#243; de Transport:';
$_['entry_status']		= 'Estat:';
$_['entry_sort_order']		= 'Ordre de Classificaci&#243;:';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per a modificar el total d&#039;estimaci&#243; de transport!';
