<?php
$_['heading_title']		= 'Comanda M&#237;nima';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat el Total de Comandes M&#237;nimes!';
$_['text_edit']		= 'Edit Low Order Fee Total';
$_['entry_total']		= 'Total de Comandes:';
$_['entry_fee']		= 'Quota:';
$_['entry_tax_class']		= 'Tipus d&#039;Impost:';
$_['entry_status']		= 'Estat:';
$_['entry_sort_order']		= 'Ordre de Classificaci&#243;:';
$_['help_total']		= 'The checkout total the order must reach before this order total is deactivated.';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per a modificar el total de comanda m&#237;nima!';
