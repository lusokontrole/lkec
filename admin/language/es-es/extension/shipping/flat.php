<?php
$_['heading_title']		= 'Tarifa Plana';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#232;xit: Heu Modificat Tarifa Plana!';
$_['text_edit']		= 'Edit Flat Rate Shipping';
$_['entry_cost']		= 'Cost:';
$_['entry_tax_class']		= 'Classe Impost:';
$_['entry_geo_zone']		= 'Zona Geogr&#224;fica:';
$_['entry_status']		= 'Estat:';
$_['entry_sort_order']		= 'Ordre de Classificaci&#243;:';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per a modificar tarifa plana d&#039;enviament!';
