<?php
$_['heading_title']		= 'Per Producte';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat les Taxes d&#039;Enviament per Article!';
$_['text_edit']		= 'Edit Per Item Shipping';
$_['entry_cost']		= 'Cost:';
$_['entry_tax_class']		= 'Classe Impost:';
$_['entry_geo_zone']		= 'Zone Geogr&#224;fica:';
$_['entry_status']		= 'Estat:';
$_['entry_sort_order']		= 'Ordre de Classificaci&#243;:';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per modificar les taxes d&#039;enviament per article!';
