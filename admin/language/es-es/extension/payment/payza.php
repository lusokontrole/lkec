<?php
$_['heading_title']		= 'Payza';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Ha modificat Payza detalls del compte!';
$_['text_edit']		= 'Edit Payza';
$_['entry_merchant']		= 'Comerciant ID:';
$_['entry_security']		= 'Codi de Seguretat:';
$_['entry_callback']		= 'URL d&#039;Alerta:<br /><span class="help">Aix&#242; s&#039;ha d&#039;ajustar al tauler de control Payza. Vost&#232; tamb&#233; haur&#224; de comprovar el "IPN Estat" a habilitat.</span>';
$_['entry_total']		= 'Total:<br /><span class="help">El total de l&#039;ordre de pagament i enviament ha d&#039;arribar abans que aquesta forma de pagament s&#039;activa.</span>';
$_['entry_order_status']		= 'Estat d&#039;Ordre:';
$_['entry_geo_zone']		= 'Zone Geogr&#224;fica:';
$_['entry_status']		= 'Estat:';
$_['entry_sort_order']		= 'Ordre de Classificaci&#243;:';
$_['help_callback']		= 'This has to be set in the Payza control panel. You will also need to check the "IPN Status" to enabled.';
$_['help_total']		= 'The checkout total the order must reach before this payment method becomes active.';
$_['error_permission']		= 'Advert&#232;ncia: No tens perm&#237;s per modificar pagament Payza!';
$_['error_merchant']		= 'Comerciant ID Requerid!';
$_['error_security']		= 'Codi de Seguretat Requerida!';
