<?php
$_['heading_title']		= 'Paymate';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat el Compte Paymate!';
$_['text_edit']		= 'Edit Paymate';
$_['text_paymate']		= '<img src="view/image/payment/paymate.png" alt="Paymate" title="Paymate" style="border: 1px solid #EEEEEE;" />';
$_['entry_username']		= 'Nom Usuari de Paymate:';
$_['entry_password']		= 'Contrasenya:<br/><span class="help">Nom&#233;s ha d&#039;utilitzar alguna contrasenya aleat&#242;ria. Aix&#242; s&#039;utilitza per assegurar-se que la informaci&#243; de pagament no es interfereix amb el despr&#233;s d&#039;haver estat enviat a la passarel&#183;la de pagament.</span>';
$_['entry_test']		= 'Mode de Prova:';
$_['entry_total']		= 'Total:<br/><span class="help">El total de caixa per aquesta comanda ha d&#039;arribar abans que aquest m&#232;tode de pagament es converteixe en actiu.</span>';
$_['entry_order_status']		= 'Estat de la Comanda:';
$_['entry_geo_zone']		= 'Zone Geogr&#224;fica:';
$_['entry_status']		= 'Estat:';
$_['entry_sort_order']		= 'Ordre de Classificaci&#224;:';
$_['help_password']		= 'Just use some random password. This will be used to make sure the payment information is not interfered with after being sent to the payment gateway.';
$_['help_total']		= 'The checkout total the order must reach before this payment method becomes active.';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per a modificar pagament Paymate!';
$_['error_username']		= 'Nom d&#039;Usuari Paymate (Paymate Username) requerit!';
$_['error_password']		= 'Es requereix contrasenya!';
