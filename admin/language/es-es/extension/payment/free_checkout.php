<?php
$_['heading_title']		= 'Gratu&#239;t';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat el M&#242;dul de Pagament Gratu&#239;t!';
$_['text_edit']		= 'Edit Free Checkout';
$_['entry_order_status']		= 'Estat de la Comanda:';
$_['entry_status']		= 'Estat:';
$_['entry_sort_order']		= 'Ordre de Classificaci&#243;:';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per modificar pagament gratu&#239;t!';
