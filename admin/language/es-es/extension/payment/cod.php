<?php
$_['heading_title']		= 'Contra Reembors';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Ha Modificat els Detalls de Cobres o Retorneu!';
$_['text_edit']		= 'Edit Cash On Delivery';
$_['entry_total']		= 'Total:<br/><span class="help">El total de caixa per aquesta comanda ha d&#039;arribar abans que aquest m&#232;tode de pagament es converteixe en actiu.</span>';
$_['entry_order_status']		= 'Estat de la Comanda:';
$_['entry_geo_zone']		= 'Zone Geogr&#224;fica:';
$_['entry_status']		= 'Estat:';
$_['entry_sort_order']		= 'Ordre de Classificaci&#243;:';
$_['help_total']		= 'The checkout total the order must reach before this payment method becomes active.';
$_['error_permission']		= 'Advert&#232;ncia: No t&#233; perm&#237;s per Modificar Pagament en efectiu al lliurament!';
