<?php
$_['heading_title']		= 'Transfer&#232;ncia Banc&#224;ria';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat els Detalls de Transfer&#232;ncia Banc&#224;ria!';
$_['text_edit']		= 'Edit Bank Transfer';
$_['entry_bank']		= 'Instruccions per a Transfer&#232;ncia Banc&#224;ria:';
$_['entry_total']		= 'Total:<br/><span class="help">El total de caixa per aquesta comanda ha d&#039;arribar abans que aquest m&#232;tode de pagament es converteixe en actiu.</span>';
$_['entry_order_status']		= 'Estat de la Comanda:';
$_['entry_geo_zone']		= 'Zones Geogr&#224;fica:';
$_['entry_status']		= 'Estat:';
$_['entry_sort_order']		= 'Ordre de Classificaci&#243;:';
$_['help_total']		= 'The checkout total the order must reach before this payment method becomes active.';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per modificar pagament per transfer&#232;ncia banc&#224;ria!';
$_['error_bank']		= 'Instruccions per Transfer&#232;ncia Banc&#224;ria Requerrides!';
