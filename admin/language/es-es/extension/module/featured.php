<?php
$_['heading_title']		= 'Destacat';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat Categories!';
$_['text_edit']		= 'Edit Featured Module';
$_['entry_name']		= 'Module Name';
$_['entry_product']		= 'Productes:<br/><span class="help">(Autocompleta)</span>';
$_['entry_limit']		= 'L&#237;mit:';
$_['entry_width']		= 'Width';
$_['entry_height']		= 'Height';
$_['entry_status']		= 'Estat:';
$_['help_product']		= '(Autocomplete)';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per modificar destacats!';
$_['error_name']		= 'Module Name must be between 3 and 64 characters!';
$_['error_width']		= 'Width required!';
$_['error_height']		= 'Height required!';
