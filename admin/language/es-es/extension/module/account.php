<?php
$_['heading_title']		= 'Compte';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat els M&#242;duls de Compte!';
$_['text_edit']		= 'Edit Account Module';
$_['entry_status']		= 'Estat:';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per a modificar els m&#242;duls de compte!';
