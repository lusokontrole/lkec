<?php
$_['heading_title']		= 'Especials';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat M&#242;dul Especials!';
$_['text_edit']		= 'Edit Specials Module';
$_['entry_name']		= 'Module Name';
$_['entry_limit']		= 'L&#237;mit:';
$_['entry_width']		= 'Width';
$_['entry_height']		= 'Height';
$_['entry_status']		= 'Estat:';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per modificar m&#242;dul especials!';
$_['error_name']		= 'Module Name must be between 3 and 64 characters!';
$_['error_width']		= 'Width required!';
$_['error_height']		= 'Height required!';
