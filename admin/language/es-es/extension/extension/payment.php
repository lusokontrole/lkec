<?php
$_['heading_title']		= 'Pagaments';
$_['text_success']		= 'Success: You have modified payments!';
$_['column_name']		= 'Forma de Pagament:';
$_['column_status']		= 'Estat:';
$_['column_sort_order']		= 'Ordre de Classificaci&#243;:';
$_['column_action']		= 'Acci&#243;:';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per a modificar pagaments!';
