<?php
$_['heading_title']		= 'Feeds de Productes';
$_['text_success']		= 'Success: You have modified feeds!';
$_['column_name']		= 'Nom Feed del Producte:';
$_['column_status']		= 'Estat:';
$_['column_action']		= 'Acci&#243;:';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per modificar Feeds!';
