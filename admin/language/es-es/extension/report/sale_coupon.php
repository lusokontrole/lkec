<?php
$_['heading_title']		= 'Informe Cupons';
$_['text_extension']		= 'Extensions';
$_['text_edit']		= 'Edit Coupons Report';
$_['text_success']		= 'Success: You have modified coupon report!';
$_['text_filter']		= 'Filter';
$_['column_name']		= 'Nom del Cup&#243;:';
$_['column_code']		= 'Codi:';
$_['column_orders']		= 'Comandes:';
$_['column_total']		= 'Total:';
$_['column_action']		= 'Acci&#243;:';
$_['entry_date_start']		= 'Data Inical:';
$_['entry_date_end']		= 'Data Final:';
$_['entry_status']		= 'Status';
$_['entry_sort_order']		= 'Sort Order';
$_['error_permission']		= 'Warning: You do not have permission to modify coupon report!';
