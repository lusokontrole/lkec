<?php
$_['heading_title']		= 'Acciones de Devolución';
$_['text_success']		= '¡Has modificado las acciones de devolución correctamente!';
$_['text_list']		= 'Lista de Acciones de Devolución';
$_['text_add']		= 'Agregar Acción de Devolución';
$_['text_edit']		= 'Editar Acción de Devolución';
$_['column_name']		= 'Acción de Devolución';
$_['column_action']		= 'Acción';
$_['entry_name']		= 'Nombre de la Acción de Devolución';
$_['error_permission']		= 'Advertencia: ¡No tiene permiso para modificar las acciones de devolución!';
$_['error_name']		= 'El nombre de la acción de devolución debe tener entre 3 y 64 caracteres.';
$_['error_return']		= 'Advertencia: ¡Esta acción de devolución no se puede eliminar ya que actualmente está asignada a %s productos devueltos!';
