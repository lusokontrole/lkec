<?php
$_['text_subject']		= '%s - 您的帐户已被拒绝！';
$_['text_welcome']		= '欢迎并感谢您在 %s 注册！';
$_['text_denied']		= '很遗憾，您的请求已被拒绝。如需更多信息，您可以在此处联系店主：';
$_['text_thanks']		= '谢谢，';
$_['button_contact']		= 'Contact Us';
