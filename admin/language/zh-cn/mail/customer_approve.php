<?php
$_['text_subject']		= '%s - 您的帐户已激活！';
$_['text_welcome']		= '欢迎并感谢您在 %s 注册！';
$_['text_login']		= '您的帐户现已创建，您可以通过访问我们的网站或以下 URL 使用您的电子邮件地址和密码登录：';
$_['text_service']		= '登录后，您将能够访问其他服务，包括查看过去的订单、打印发票和编辑您的帐户信息。';
$_['text_thanks']		= '谢谢，';
$_['button_login']		= 'Login';
