<?php
$_['text_subject']		= '%s - 返回更新 %s';
$_['text_return_id']		= '返回编号：';
$_['text_date_added']		= '归期：';
$_['text_return_status']		= '您的退货已更新为以下状态：';
$_['text_comment']		= '您的退货意见如下：';
$_['text_footer']		= '如果您有任何问题，请回复此电子邮件。';
