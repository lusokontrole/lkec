<?php
$_['heading_title']		= '活动';
$_['text_success']		= '成功：您已修改事件！';
$_['text_list']		= '活动列表';
$_['text_event']		= '扩展程序使用事件来覆盖商店的默认功能。如果您有问题，可以在此处禁用或启用事件。';
$_['text_info']		= '活动信息';
$_['text_trigger']		= '扳机';
$_['text_action']		= '行动';
$_['column_code']		= '事件代码';
$_['column_status']		= '地位';
$_['column_sort_order']		= '排序';
$_['column_action']		= '行动';
$_['error_permission']		= '警告：您无权修改扩展！';
