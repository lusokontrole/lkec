<?php
$_['heading_title']		= '扩展安装程序';
$_['text_progress']		= '安装进度';
$_['text_upload']		= '上传您的扩展程序';
$_['text_history']		= '安装历史';
$_['text_success']		= '成功：扩展已安装！';
$_['text_install']		= '安装';
$_['column_filename']		= '文件名';
$_['column_date_added']		= '添加日期';
$_['column_action']		= '行动';
$_['entry_upload']		= '上传文件';
$_['entry_progress']		= '进步';
$_['help_upload']		= '需要扩展名为“.ocmod.zip”的修改文件。';
$_['error_permission']		= '警告：您无权修改扩展！';
$_['error_install']		= '正在安装扩展程序，请等待几秒钟，然后再尝试安装！';
$_['error_upload']		= '文件无法上传！';
$_['error_filetype']		= '无效的文件类型！';
$_['error_file']		= '找不到文件！';
