<?php
$_['heading_title']		= '修改';
$_['text_success']		= '成功：您已经修改了修改！';
$_['text_refresh']		= '每当您启用/禁用或删除修改时，您都需要单击刷新按钮来重建您的修改缓存！';
$_['text_list']		= '修改清单';
$_['column_name']		= '修改名称';
$_['column_author']		= '作者';
$_['column_version']		= '版本';
$_['column_status']		= '地位';
$_['column_date_added']		= '添加日期';
$_['column_action']		= '行动';
$_['error_permission']		= '警告：您无权修改修改！';
