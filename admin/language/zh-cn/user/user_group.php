<?php
$_['heading_title']		= '用户组';
$_['text_success']		= '成功：您已修改用户组！';
$_['text_list']		= '用户组';
$_['text_add']		= '添加用户组';
$_['text_edit']		= '编辑用户组';
$_['column_name']		= '用户组名称';
$_['column_action']		= '行动';
$_['entry_name']		= '用户组名称';
$_['entry_access']		= '访问权限';
$_['entry_modify']		= '修改权限';
$_['error_permission']		= '警告：您无权修改用户组！';
$_['error_name']		= '用户组名称必须介于 3 到 64 个字符之间！';
$_['error_user']		= '警告：无法删除此用户组，因为它当前已分配给 %s 用户！';
