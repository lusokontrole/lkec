<?php
$_['heading_title']		= '主题编辑器';
$_['text_success']		= '成功：您已修改主题！';
$_['text_edit']		= '编辑主题';
$_['text_store']		= '选择您的商店';
$_['text_template']		= '选择一个模板';
$_['text_default']		= '默认';
$_['text_history']		= '主题历史';
$_['text_twig']		= '主题编辑器使用模板语言 Twig。您可以在此处阅读<a href="http://twig.sensiolabs.org/documentation" target="_blank" class="alert-link">Twig 语法</a>。';
$_['column_store']		= '店铺';
$_['column_route']		= '路线';
$_['column_theme']		= '主题';
$_['column_date_added']		= '添加日期';
$_['column_action']		= '行动';
$_['error_permission']		= '警告：您没有修改主题编辑器的权限！';
$_['error_twig']		= '警告：您只能保存 .twig 文件！';
