<?php
$_['heading_title']		= '属性组';
$_['text_success']		= '成功：您已经修改了属性组！';
$_['text_list']		= '属性组列表';
$_['text_add']		= '添加属性组';
$_['text_edit']		= '编辑属性组';
$_['column_name']		= '属性组名称';
$_['column_sort_order']		= '排序';
$_['column_action']		= '行动';
$_['entry_name']		= '属性组名称';
$_['entry_sort_order']		= '排序';
$_['error_warning']		= 'Warning: Please check the form carefully for errors!';
$_['error_permission']		= '警告：您无权修改属性组！';
$_['error_name']		= '属性组名称必须介于 1 到 64 个字符之间！';
$_['error_attribute']		= '警告：无法删除此属性组，因为它当前已分配给 %s 属性！';
$_['error_product']		= '警告：无法删除此属性组，因为它当前已分配给 %s 产品！';
