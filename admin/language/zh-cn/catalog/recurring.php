<?php
$_['heading_title']		= '重复配置文件';
$_['text_success']		= '成功：您已修改重复配置文件！';
$_['text_list']		= '重复配置文件列表';
$_['text_add']		= '添加重复配置文件';
$_['text_edit']		= '编辑定期资料';
$_['text_day']		= '天';
$_['text_week']		= '星期';
$_['text_semi_month']		= '半月';
$_['text_month']		= '月';
$_['text_year']		= '年';
$_['text_recurring']		= '<p><i class="fa fa-info-circle"></i> 经常性金额按频率和周期计算。</p><p>例如，如果您使用频率“周”和周期为“2”，则每 2 周向用户收费一次。</p><p>持续时间是用户进行付款的次数，如果您希望在取消之前付款，请将其设置为 0。 </p>';
$_['text_profile']		= '经常性资料';
$_['text_trial']		= '试用简介';
$_['entry_name']		= '姓名';
$_['entry_price']		= '价格';
$_['entry_duration']		= '期间';
$_['entry_cycle']		= '循环';
$_['entry_frequency']		= '频率';
$_['entry_trial_price']		= '试用价';
$_['entry_trial_duration']		= '试用期';
$_['entry_trial_status']		= '试用状态';
$_['entry_trial_cycle']		= '试用周期';
$_['entry_trial_frequency']		= '试用频率';
$_['entry_status']		= '地位';
$_['entry_sort_order']		= '排序';
$_['column_name']		= '姓名';
$_['column_sort_order']		= '排序';
$_['column_action']		= '行动';
$_['button_report_print']		= 'Print Recurring List';
$_['error_warning']		= '警告：请仔细检查表格是否有错误！';
$_['error_permission']		= '警告：您无权修改定期配置文件！';
$_['error_name']		= '配置文件名称必须大于 3 且小于 255 个字符！';
$_['error_product']		= '警告：无法删除此重复配置文件，因为它当前已分配给 %s 产品！';
