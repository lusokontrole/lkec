<?php
$_['heading_title']		= '制造商';
$_['text_success']		= '成功：您已经修改了制造商！';
$_['text_list']		= '制造商列表';
$_['text_add']		= '添加制造商';
$_['text_edit']		= '编辑制造商';
$_['text_default']		= '默认';
$_['text_keyword']		= '不要使用空格，而是将空格替换为 - 并确保 SEO URL 是全局唯一的。';
$_['column_image']		= 'Image';
$_['column_name']		= '生产商名称';
$_['column_sort_order']		= '排序';
$_['column_action']		= '行动';
$_['entry_name']		= '生产商名称';
$_['entry_store']		= '专卖店';
$_['entry_keyword']		= '关键词';
$_['entry_image']		= '图片';
$_['entry_sort_order']		= '排序';
$_['entry_layout']		= 'Layout Override';
$_['error_warning']		= 'Warning: Please check the form carefully for errors!';
$_['error_permission']		= '警告：您无权修改制造商！';
$_['error_name']		= '制造商名称必须介于 1 到 64 个字符之间！';
$_['error_keyword']		= 'SEO URL 已在使用中！';
$_['error_unique']		= 'SEO URL 必须是唯一的！';
$_['error_product']		= '警告：无法删除此制造商，因为它当前已分配给 %s 产品！';
