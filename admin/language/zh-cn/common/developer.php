<?php
$_['heading_title']		= '开发者设置';
$_['text_success']		= '成功：您已修改开发者设置！';
$_['text_theme']		= '主题';
$_['text_sass']		= 'SASS';
$_['text_cache']		= '成功：您已清除 %s 缓存！';
$_['column_component']		= '零件';
$_['column_action']		= '行动';
$_['entry_theme']		= '主题';
$_['entry_sass']		= 'SASS';
$_['entry_cache']		= '缓存';
$_['button_on']		= '上';
$_['button_off']		= '离开';
$_['error_permission']		= '警告：您无权修改开发者设置！';
