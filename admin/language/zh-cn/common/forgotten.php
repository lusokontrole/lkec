<?php
$_['heading_title']		= '忘记密码了吗？';
$_['text_forgotten']		= '忘记密码';
$_['text_your_email']		= '您的电子邮件地址';
$_['text_email']		= '输入与您的帐户关联的电子邮件地址。单击提交以通过电子邮件将密码重置链接发送给您。';
$_['text_success']		= '一封带有确认链接的电子邮件已发送至您的管理员电子邮件地址。';
$_['entry_email']		= '电子邮件地址';
$_['entry_password']		= '新密码';
$_['entry_confirm']		= '确认';
$_['error_email']		= '警告：我们的记录中没有找到该电子邮件地址，请重试！';
$_['error_password']		= '密码必须在 4 到 20 个字符之间！';
$_['error_confirm']		= '密码和密码确认不符！';
