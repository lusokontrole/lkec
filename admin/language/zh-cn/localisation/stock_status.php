<?php
$_['heading_title']		= '库存状态';
$_['text_success']		= '成功：您已修改库存状态！';
$_['text_list']		= '库存状态列表';
$_['text_add']		= '添加库存状态';
$_['text_edit']		= '编辑库存状态';
$_['column_name']		= '库存状态名称';
$_['column_action']		= '行动';
$_['entry_name']		= '库存状态名称';
$_['error_permission']		= '警告：您无权修改库存状态！';
$_['error_name']		= '库存状态名称必须介于 3 到 32 个字符之间！';
$_['error_product']		= '警告：无法删除此库存状态，因为它当前已分配给 %s 产品！';
