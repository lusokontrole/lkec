<?php
$_['heading_title']		= '地理区域';
$_['text_success']		= '成功：您已修改地理区域！';
$_['text_list']		= '地理区域列表';
$_['text_add']		= '添加地理区域';
$_['text_edit']		= '编辑地理区域';
$_['text_geo_zone']		= '地理区域';
$_['column_name']		= '地理区域名称';
$_['column_description']		= '描述';
$_['column_action']		= '行动';
$_['entry_name']		= '地理区域名称';
$_['entry_description']		= '描述';
$_['entry_country']		= '国家';
$_['entry_zone']		= '区';
$_['error_permission']		= '警告：您无权修改地理区域！';
$_['error_name']		= '地理区域名称必须介于 3 到 32 个字符之间！';
$_['error_description']		= '描述 名称必须介于 3 到 255 个字符之间！';
$_['error_tax_rate']		= '警告：此地理区域无法删除，因为它当前已分配给一个或多个税率！';
