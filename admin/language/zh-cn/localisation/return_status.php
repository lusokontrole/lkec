<?php
$_['heading_title']		= '返回状态';
$_['text_success']		= '成功：您已修改退货状态！';
$_['text_list']		= '返回状态列表';
$_['text_add']		= '添加退货状态';
$_['text_edit']		= '编辑退货状态';
$_['column_name']		= '返回状态名称';
$_['column_action']		= '行动';
$_['entry_name']		= '返回状态名称';
$_['error_permission']		= '警告：您无权修改退货状态！';
$_['error_name']		= '返回状态名称必须介于 3 到 32 个字符之间！';
$_['error_default']		= '警告：此退货状态无法删除，因为它当前被指定为默认退货状态！';
$_['error_return']		= '警告：无法删除此退货状态，因为它当前已分配给 %s 退货！';
