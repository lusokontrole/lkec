<?php
$_['heading_title']		= '语言';
$_['text_success']		= '成功：您已经修改了语言！';
$_['text_list']		= '语言列表';
$_['text_add']		= '添加语言';
$_['text_edit']		= '编辑语言';
$_['column_name']		= '语言名称';
$_['column_code']		= '代码';
$_['column_sort_order']		= '排序';
$_['column_action']		= '行动';
$_['entry_name']		= '语言名称';
$_['entry_code']		= '代码';
$_['entry_locale']		= '语言环境';
$_['entry_status']		= '地位';
$_['entry_sort_order']		= '排序';
$_['help_locale']		= '示例：en_US.UTF-8,en_US,en-gb,en_gb,english';
$_['help_status']		= '在语言下拉列表中隐藏/显示';
$_['error_permission']		= '警告：您无权修改语言！';
$_['error_exists']		= '警告：您在语言之前添加了！';
$_['error_name']		= '语言名称必须介于 3 到 32 个字符之间！';
$_['error_code']		= '语言代码必须至少 2 个字符！';
$_['error_locale']		= '需要语言环境！';
$_['error_default']		= '警告：无法删除此语言，因为它当前已指定为默认商店语言！';
$_['error_admin']		= '警告：无法删除此语言，因为它当前被指定为管理语言！';
$_['error_store']		= '警告：无法删除此语言，因为它当前已分配给 %s 商店！';
$_['error_order']		= '警告：无法删除此语言，因为它当前已分配给 %s 个订单！';
