<?php
$_['heading_title']		= '返回操作';
$_['text_success']		= '成功：您已修改返回操作！';
$_['text_list']		= '返回操作列表';
$_['text_add']		= '添加退货操作';
$_['text_edit']		= '编辑退货操作';
$_['column_name']		= '返回动作名称';
$_['column_action']		= '行动';
$_['entry_name']		= '返回动作名称';
$_['error_permission']		= '警告：您无权修改退货操作！';
$_['error_name']		= '返回操作名称必须介于 3 到 64 个字符之间！';
$_['error_return']		= '警告：无法删除此退货操作，因为它当前已分配给 %s 退货产品！';
