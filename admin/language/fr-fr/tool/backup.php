<?php
$_['heading_title']		= 'Sauvegarde et restauration';
$_['text_success']		= 'Succès : la base de données a été correctement sauvegardée !';
$_['entry_progress']		= 'Progress';
$_['entry_export']		= 'Exporter une sauvegarde';
$_['tab_backup']		= 'Backup';
$_['tab_restore']		= 'Restore';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de modifier les sauvegardes et restaurations !';
$_['error_export']		= 'Attention : vous devez sélectionner au moins une table à exporter !';
$_['error_file']		= 'File could not be found!';
