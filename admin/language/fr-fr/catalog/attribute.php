<?php
$_['heading_title']		= 'Attributs';
$_['text_success']		= 'Les attributs ont correctement été sauvegardés.';
$_['text_list']		= 'Liste des attributs';
$_['text_add']		= 'Ajouter un attribut';
$_['text_edit']		= 'Modifier l’attribut';
$_['column_name']		= 'Nom de l’attribut';
$_['column_attribute_group']		= 'Groupe d’attributs';
$_['column_sort_order']		= 'Classement';
$_['column_action']		= 'Action';
$_['entry_name']		= 'Nom de l’attribut';
$_['entry_attribute_group']		= 'Groupe d’attributs';
$_['entry_sort_order']		= 'Classement';
$_['error_warning']		= 'Warning: Please check the form carefully for errors!';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de modifier les attributs !';
$_['error_attribute_group']		= 'Un groupe d’attributs est requis !';
$_['error_name']		= 'Le nom de l’attribut doit contenir entre 3 et 64 caractères !';
$_['error_product']		= 'Attention : cet attribut ne peut être supprimé car il est actuellement attribué à %s produits !';
