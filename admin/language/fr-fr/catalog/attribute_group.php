<?php
$_['heading_title']		= 'Groupe d’attributs';
$_['text_success']		= 'Les groupes d’attributs ont correctement été modifiés !';
$_['text_list']		= 'Liste des groupes d’attributs';
$_['text_add']		= 'Ajouter un groupe d’attributs';
$_['text_edit']		= 'Modifier un groupe d’attributs';
$_['column_name']		= 'Nom du groupe d’attributs';
$_['column_sort_order']		= 'Classement';
$_['column_action']		= 'Action';
$_['entry_name']		= 'Nom du groupe d’attributs';
$_['entry_sort_order']		= 'Classement';
$_['error_warning']		= 'Warning: Please check the form carefully for errors!';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de modifier les groupes d’attributs !';
$_['error_name']		= 'Attention : le nom du groupe d’attributs doit contenir entre 3 et 64 caractères !';
$_['error_attribute']		= 'Attention : ce groupe d’attributs ne peut être supprimé car il est actuellement attribué à %s attributs !';
$_['error_product']		= 'Attention : ce groupe d’attributs ne peut être supprimé car il est actuellement attribué à %s produits !';
