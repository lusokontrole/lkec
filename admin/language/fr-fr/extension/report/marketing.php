<?php
$_['heading_title']		= 'Rapport de marketing';
$_['text_extension']		= 'Extensions';
$_['text_edit']		= 'Edit Marketing Report';
$_['text_success']		= 'Success: You have modified marketing report!';
$_['text_filter']		= 'Filter';
$_['text_all_status']		= 'Tous les statuts';
$_['column_campaign']		= 'Nom de la campagne';
$_['column_code']		= 'Code';
$_['column_clicks']		= 'Clics';
$_['column_orders']		= 'Commandes n°';
$_['column_total']		= 'Total';
$_['entry_date_start']		= 'Date de début';
$_['entry_date_end']		= 'Date de fin';
$_['entry_order_status']		= 'Order Status';
$_['entry_status']		= 'Statut de la commande';
$_['entry_sort_order']		= 'Sort Order';
$_['error_permission']		= 'Warning: You do not have permission to modify marketing report!';
