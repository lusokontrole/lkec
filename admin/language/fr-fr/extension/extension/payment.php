<?php
$_['heading_title']		= 'Paiement';
$_['text_success']		= 'Success: You have modified payments!';
$_['column_name']		= 'Mode de paiement';
$_['column_status']		= '&Eacute;tat';
$_['column_sort_order']		= 'Classement';
$_['column_action']		= 'Action';
$_['error_permission']		= 'Attention, vous n&#8217;avez la permission de modifier les <b>Paiements</b> !';
