<?php
$_['heading_title']		= 'Flux de produits';
$_['text_success']		= 'Success: You have modified feeds!';
$_['column_name']		= 'Nom du flux de produits';
$_['column_status']		= '&Eacute;tat';
$_['column_action']		= 'Action';
$_['error_permission']		= 'Attention, vous n&#8217;avez la permission de modifier le <b>Flux de produits</b> !';
