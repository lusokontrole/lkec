<?php
$_['heading_title']		= 'Retrait au Magasin';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'F&eacute;licitations, vous avez modifi&eacute; le <b>Retrait au Magasin</b> avec succ&egrave;s !';
$_['text_edit']		= 'Edit Pickup From Store Shipping';
$_['entry_geo_zone']		= 'Zone g&eacute;ographique :';
$_['entry_status']		= '&Eacute;tat :';
$_['entry_sort_order']		= 'Classement :';
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier le <b>Retrait au Magasin</b> !';
