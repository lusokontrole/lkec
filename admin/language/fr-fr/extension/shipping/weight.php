<?php
$_['heading_title']		= 'Livraison Bas&eacute;e sur le Poids';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'F&eacute;licitations, vous avez modifi&eacute; la <b>Livraison bas&eacute;e sur le poids</b> avec succ&egrave;s !';
$_['text_edit']		= 'Edit Weight Based Shipping';
$_['entry_rate']		= 'Taux : <br /><span class="help">Exemple : 5:10.00,7:12.00 Poids:Co&ucirc;t,Poids:Co&ucirc;t, etc..</span>';
$_['entry_tax_class']		= 'Classe de Taxe :';
$_['entry_geo_zone']		= 'Zone g&eacute;ographique :';
$_['entry_status']		= '&Eacute;tat :';
$_['entry_sort_order']		= 'Classement :';
$_['help_rate']		= 'Example: 5:10.00,7:12.00 Weight:Cost,Weight:Cost, etc..';
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier la <b>Livraison bas&eacute;e sur le poids</b> !';
