<?php
$_['heading_title']		= 'Taux Fixe';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'F&eacute;licitations, vous avez modifi&eacute; la livraison &agrave; <b>Taux fixe</b> avec succ&egrave;s !';
$_['text_edit']		= 'Edit Flat Rate Shipping';
$_['entry_cost']		= 'Co&ucirc;t :';
$_['entry_tax_class']		= 'Classe de taxes :';
$_['entry_geo_zone']		= 'Zone g&eacute;ographique :';
$_['entry_status']		= '&Eacute;tat :';
$_['entry_sort_order']		= 'Classement :';
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier la livraison &agrave; <b>Taux fixe</b> !';
