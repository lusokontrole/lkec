<?php
$_['heading_title']		= 'Total';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'F&eacute;licitations, vous avez modifi&eacute; le <b>Total</b> avec succ&egrave;s !';
$_['text_edit']		= 'Edit Total Total';
$_['entry_status']		= '&Eacute;tat :';
$_['entry_sort_order']		= 'Classement :';
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier le <b>Total</b> !';
