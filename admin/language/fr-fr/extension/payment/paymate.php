<?php
$_['heading_title']		= 'PayMate';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'F&eacute;licitations, vous avez modifi&eacute; les d&eacute;tails du paiement <b>PayMate</b> avec succ&egrave;s !';
$_['text_edit']		= 'Edit Paymate';
$_['text_paymate']		= '<img src="view/image/payment/paymate.png" alt="Paymate" title="Paymate" style="border: 1px solid #EEEEEE;" />';
$_['entry_username']		= 'Nom d&#8217;utilisateur :';
$_['entry_password']		= 'Mot de passe :<br /><span class="help">Il suffit d&8217;utiliser un certain mot de passe al&eacute;atoire. Celui-ci sera utilis&eacute;e pour s&8217;assurer que les informations de paiement ne soit pas entrav&eacute;e apr&egrave;s avoir &eacute;t&eacute; envoy&eacute; &agrave; la passerelle de paiement.</span>';
$_['entry_test']		= 'Mode test :';
$_['entry_total']		= 'Total :<br /><span class="help">Le total de la commande doit &ecirc;tre calcul&eacute; avant que ce mode de paiement ne soit actif.</span>';
$_['entry_order_status']		= '&Eacute;tat de la commande :';
$_['entry_geo_zone']		= 'Zone g&eacute;ographique :';
$_['entry_status']		= '&Eacute;tat :';
$_['entry_sort_order']		= 'Classement :';
$_['help_password']		= 'Just use some random password. This will be used to make sure the payment information is not interfered with after being sent to the payment gateway.';
$_['help_total']		= 'The checkout total the order must reach before this payment method becomes active.';
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier le paiement <b>PayMate</b> !';
$_['error_username']		= 'Attention, le nom d&#8217;utilisateur est requis !';
$_['error_password']		= 'Attention, le mot de passe est requis !';
