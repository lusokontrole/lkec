<?php
$_['heading_title']		= 'Virement bancaire';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'F&eacute;licitations, vous avez modifi&eacute; les d&eacute;tails du <b>Virement bancaire</b> avec succ&egrave;s !';
$_['text_edit']		= 'Edit Bank Transfer';
$_['entry_bank']		= 'Instructions pour le virement bancaire :';
$_['entry_total']		= 'Total :<br /><span class="help">Le total de la commande doit &ecirc;tre calcul&eacute; avant que ce mode de paiement ne soit actif.</span>';
$_['entry_order_status']		= '&Eacute;tat de la commande :';
$_['entry_geo_zone']		= 'Zone g&eacute;ographique :';
$_['entry_status']		= '&Eacute;tat :';
$_['entry_sort_order']		= 'Classement :';
$_['help_total']		= 'The checkout total the order must reach before this payment method becomes active.';
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier le paiement par <b>Virement bancaire</b> !';
$_['error_bank']		= 'Attention, les instructions pour le virement bancaire sont requises !';
