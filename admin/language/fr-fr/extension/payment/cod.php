<?php
$_['heading_title']		= '&Agrave; la livraison';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'F&eacute;licitations, vous avez modifi&eacute; le paiement <b>&Agrave; la livraison</b> avec succ&egrave;s !';
$_['text_edit']		= 'Edit Cash On Delivery';
$_['entry_total']		= 'Total :<br /><span class="help">Le total de la commande doit &ecirc;tre calcul&eacute; avant que ce mode de paiement ne soit actif.</span>';
$_['entry_order_status']		= '&Eacute;tat de la commande :';
$_['entry_geo_zone']		= 'Zone g&eacute;ographique :';
$_['entry_status']		= '&Eacute;tat :';
$_['entry_sort_order']		= 'Classement :';
$_['help_total']		= 'The checkout total the order must reach before this payment method becomes active.';
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier le paiement <b>&Agrave; la livraison</b> !';
