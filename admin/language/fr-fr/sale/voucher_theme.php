<?php
$_['heading_title']		= 'Thèmes des bons d’achat';
$_['text_success']		= 'Succès : vous avez modifié les thèmes de bons d’achat !';
$_['text_list']		= 'Liste des thème de bons d’achat';
$_['text_add']		= 'Ajouter un thème de bons d’achat';
$_['text_edit']		= 'Modifier un thème de bons d’achat';
$_['column_name']		= 'Nom du thème de bons d’achat';
$_['column_action']		= 'Action';
$_['entry_name']		= 'Nom du thème de bons d’achat';
$_['entry_description']		= 'Description du thème de bons d’achat';
$_['entry_image']		= 'Image';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de modifier les thèmes des bons d’achat !';
$_['error_name']		= 'Le nom du thème de bons d’achat doit contenir entre 3 et 32 caractères !';
$_['error_image']		= 'Une image requise !';
$_['error_voucher']		= 'Attention : ce thème de bon d’achat ne peut pas être supprimé car il est actuellement attribué à %s bons d’achat !';
