<?php
$_['heading_title']		= 'Affiliés';
$_['text_success']		= 'Succès : vous avez modifié des affiliés !';
$_['text_list']		= 'Liste d’affiliation';
$_['text_add']		= 'Ajouter un affilié';
$_['text_edit']		= 'Modifier un affilié';
$_['text_affiliate']		= 'Affiliate Details';
$_['text_payment']		= 'Payment Details';
$_['text_other']		= 'Other';
$_['text_balance']		= 'Solde';
$_['text_cheque']		= 'Chèque';
$_['text_paypal']		= 'PayPal';
$_['text_bank']		= 'Virement bancaire';
$_['text_history']		= 'History';
$_['text_history_add']		= 'Add History';
$_['text_transaction']		= 'Transactions';
$_['text_transaction_add']		= 'Add Transaction';
$_['text_report']		= 'Report';
$_['text_filter']		= 'Filter';
$_['column_name']		= 'Nom de l’affilié';
$_['column_tracking']		= 'Tracking';
$_['column_commission']		= 'Commission';
$_['column_balance']		= 'Solde';
$_['column_status']		= 'Statut';
$_['column_ip']		= 'IP';
$_['column_account']		= 'Accounts';
$_['column_store']		= 'Store';
$_['column_country']		= 'Country';
$_['column_date_added']		= 'Date d’ajout';
$_['column_comment']		= 'Comment';
$_['column_description']		= 'Description';
$_['column_amount']		= 'Montant';
$_['column_action']		= 'Action';
$_['entry_customer']		= 'Customer';
$_['entry_status']		= 'Statut';
$_['entry_company']		= 'Société';
$_['entry_tracking']		= 'Tracking Code';
$_['entry_website']		= 'Site Internet';
$_['entry_commission']		= 'Commission (%)';
$_['entry_tax']		= 'Identification fiscale';
$_['entry_payment']		= 'Moyen de paiement';
$_['entry_cheque']		= 'Nom de bénéficiaire du chèque';
$_['entry_paypal']		= 'Adresse électronique du compte PayPal';
$_['entry_bank_name']		= 'Nom de la banque';
$_['entry_bank_branch_number']		= 'Code ABA/BSB (numéro de succursale)';
$_['entry_bank_swift_code']		= 'Code SWIFT';
$_['entry_bank_account_name']		= 'Nom du compte';
$_['entry_bank_account_number']		= 'Numéro du compte';
$_['entry_comment']		= 'Comment';
$_['entry_description']		= 'Description';
$_['entry_amount']		= 'Montant';
$_['entry_date_added']		= 'Date d’ajout';
$_['help_tracking']		= 'The tracking code that will be used to track referrals.';
$_['help_commission']		= 'Pourcentage que reçoit l’affilié sur chaque commande.';
$_['error_warning']		= 'Attention : veuillez vérifier attentivement les erreurs dans le formulaire !';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de modifier les affiliés !';
$_['error_customer']		= 'Warning: Customer required!';
$_['error_already']		= 'Warning: Customer is already registered as an affiliate!';
$_['error_tracking']		= 'Tracking Code required!';
$_['error_exists']		= 'Attention : cette adresse électronique est déjà enregistrée !';
$_['error_cheque']		= 'Le nom du bénéficiaire du chèque est requis !';
$_['error_paypal']		= 'L’adresse électronique PayPal ne semble pas être valide !';
$_['error_bank_account_name']		= 'Le nom de compte est requis !';
$_['error_bank_account_number']		= 'Le numéro de compte est requis !';
$_['error_custom_field']		= '%s required!';
