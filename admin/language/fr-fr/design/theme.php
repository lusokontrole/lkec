<?php
$_['heading_title']		= 'Éditeur de thème';
$_['text_success']		= 'Vous venez de modifier les thèmes !';
$_['text_edit']		= 'Modifier le thème';
$_['text_store']		= 'Choisir la boutique';
$_['text_template']		= 'Choisir le modèle';
$_['text_default']		= 'Par défaut';
$_['text_history']		= 'Theme History';
$_['text_twig']		= 'The theme editor uses the template language Twig. You can read about <a href="https://twig.symfony.com/doc/" target="_blank" class="alert-link">Twig syntax here</a>.';
$_['column_store']		= 'Store';
$_['column_route']		= 'Route';
$_['column_theme']		= 'Theme';
$_['column_date_added']		= 'Date Added';
$_['column_action']		= 'Action';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de modifier l’éditeur de thème !';
$_['error_twig']		= 'Warning: You can only save .twig files!';
