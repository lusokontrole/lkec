<?php
$_['heading_title']		= 'Statuts des retours';
$_['text_success']		= 'Vous venez de modifier les statuts de retour !';
$_['text_list']		= 'Liste des statuts de retour';
$_['text_add']		= 'Ajouter un statut de retour';
$_['text_edit']		= 'Modifier un statut de retour';
$_['column_name']		= 'Nom du statut de retour';
$_['column_action']		= 'Action';
$_['entry_name']		= 'Nom du statut de retour';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de modifier les statuts de retour !';
$_['error_name']		= 'Le nom du statut de retour doit contenir entre 3 et 32 caractères !';
$_['error_default']		= 'Attention : ce statut de retour ne peut être supprimé car il est actuellement assigné comme statut de retour par défaut !';
$_['error_return']		= 'Attention : ce statut de retour ne peut être supprimé car il est actuellement attribué à %s retours !';
