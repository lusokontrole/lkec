<?php
$_['heading_title']		= 'Zones géographiques';
$_['text_success']		= 'Vous venez de modifier les zones géographiques !';
$_['text_list']		= 'Liste des zones géographiques';
$_['text_add']		= 'Ajouter une zone géographique';
$_['text_edit']		= 'Modifier une zone géographique';
$_['text_geo_zone']		= 'Geo Zones';
$_['column_name']		= 'Nom de la zone géographique';
$_['column_description']		= 'Description';
$_['column_action']		= 'Action';
$_['entry_name']		= 'Nom de la zone géographique';
$_['entry_description']		= 'Description';
$_['entry_country']		= 'Pays';
$_['entry_zone']		= 'Zone';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de modifier les zones géographiques !';
$_['error_name']		= 'Le nom de la zone géographique doit contenir entre 3 et 32 caractères !';
$_['error_description']		= 'La description de la zone géographique doit contenir entre 3 et 255 caractères !';
$_['error_tax_rate']		= 'Attention : cette zone géographique ne peut être supprimée car elle est actuellement attribuée à un ou plusieurs taux de TVA !';
