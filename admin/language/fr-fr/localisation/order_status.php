<?php
$_['heading_title']		= 'Statuts de commande';
$_['text_success']		= 'Vous venez de modifier les statuts de commande !';
$_['text_list']		= 'Liste des statuts de commande';
$_['text_add']		= 'Ajouter un statut de commande';
$_['text_edit']		= 'Modifier un statut de commande';
$_['column_name']		= 'Nom du statut de commande';
$_['column_action']		= 'Action';
$_['entry_name']		= 'Nom du statut de commande';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de modifier les statuts de commande !';
$_['error_name']		= 'Le nom du statut de commande doit contenir entre 3 et 32 caractères !';
$_['error_default']		= 'Attention : ce statut de commande ne peut être supprimé car il est actuellement assigné comme le statut de commande par défaut !';
$_['error_download']		= 'Attention : ce statut de commande ne peut être supprimé car il est actuellement assigné comme le statut de commande par défaut pour les téléchargements !';
$_['error_store']		= 'Attention : ce statut de commande ne peut être supprimé car il est actuellement attribué à %s magasins !';
$_['error_order']		= 'Attention : ce statut de commande ne peut être supprimé car il est actuellement attribué à %s commandes !';
