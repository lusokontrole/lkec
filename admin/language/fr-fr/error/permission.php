<?php
$_['heading_title']		= 'Permission refusée !';
$_['text_permission']		= 'Vous n’avez pas la permission d’accéder à cette page. Veuillez vous adresser à votre administrateur système.';
