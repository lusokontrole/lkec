<?php
$_['heading_title']		= 'Anpassungen';
$_['text_success']		= 'Einstellungen erfolgreich bearbeitet';
$_['text_refresh']		= 'Nach Aktivierung oder Deaktivierung einer Anpassung immer danach den Button <b>Aktualisieren</b> anklicken um den Cache neu zu erstellen.<br><b>Hinweise</b><ul><li>Um eine Anpassung nur zu <b>deaktivieren</b>, den roten Button in der Zeile der Anpassung anklicken.<br>Ebenso dann in den jeweiligen Einstellungen überprüfen, ob die Erweiterung nicht mehr aktiv ist.</li><li>Um eine Anpassung <b>vollständig zu löschen</b> (d.h. Datenbankeinträge, Einstellungen und Dateien), zuerst die Anpassung deaktivieren.<br>Danach die Checkbox markieren und abschließend auf den roten Button rechts oben klicken.</li></ul>';
$_['text_list']		= 'Übersicht';
$_['column_name']		= 'Name';
$_['column_author']		= 'Autor';
$_['column_version']		= 'Version';
$_['column_status']		= 'Status';
$_['column_date_added']		= 'Hinzugefügt';
$_['column_action']		= 'Aktion';
$_['error_permission']		= 'Keine Rechte für diese Aktion';
