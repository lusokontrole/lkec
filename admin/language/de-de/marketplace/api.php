<?php
$_['heading_title']		= 'Souk Marktplatz API';
$_['text_success']		= 'Einstellungen erfolgreich bearbeitet';
$_['text_signup']		= 'Bitte Souk API angeben welche <a href="https://khadija.agency/index.php?route=account/store" target="_blank" class="alert-link">hier abgerufen</a> werden kann';
$_['entry_username']		= 'Benutzername';
$_['entry_secret']		= 'Geheimbegriff';
$_['error_permission']		= 'Keine Rechte für diese Aktion';
$_['error_username']		= 'Benutzername erforderlich';
$_['error_secret']		= 'Geheimbegriff erforderlich';
