<?php
$_['heading_title']		= 'Installer';
$_['text_progress']		= 'Installationsfortschritt<br><i style="font-size:12px;">(Nach der Installation muss über das Menü <b>Anpassungen</b> eine Aktualisierung durchgeführt werden um die Änderungen anzuwenden!)</i>';
$_['text_upload']		= 'Erweiterung hochladen';
$_['text_history']		= 'Verlauf';
$_['text_success']		= 'Erweiterung erfolgreich installiert';
$_['text_install']		= 'Installiere ..';
$_['column_filename']		= 'Dateiname';
$_['column_date_added']		= 'Hinzugefügt';
$_['column_action']		= 'Aktion';
$_['entry_upload']		= 'Datei hochladen';
$_['entry_progress']		= 'Weiter';
$_['help_upload']		= 'Die Endung des Erweiterungspaketes muss auf <b>.ocmod.zip</b> lauten';
$_['error_permission']		= 'Keine Rechte für diese Aktion';
$_['error_install']		= 'Installation in Arbeit .. bitte kurz warten ..';
$_['error_upload']		= 'Datei konnte nicht hochgeladen werden';
$_['error_filetype']		= 'Ungültige Dateiart';
$_['error_file']		= 'Datei konnte nicht gefunden werden';
