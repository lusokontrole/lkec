<?php
$_['heading_title']		= 'Statistiken';
$_['text_success']		= 'Datensatz erfolgreich bearbeitet';
$_['text_list']		= 'Übersicht';
$_['text_order_sale']		= 'Verkäufe';
$_['text_order_processing']		= 'Aufträge <b style="color:orange">in Arbeit</b>';
$_['text_order_complete']		= 'Aufträge <b style="color:green">abgeschlossen</b>';
$_['text_order_other']		= 'Aufträge weitere Status';
$_['text_return']		= 'Returns';
$_['text_customer']		= 'Kunden <b style="color:orange">warten auf Überprüfung</b>';
$_['text_affiliate']		= 'Partner warten auf Überprüfung';
$_['text_product']		= 'Produkte <b style="color:red">ni. lagernd</b>';
$_['text_review']		= 'Bewertungen <b style="color:orange">warten auf Überprüfung</b>';
$_['column_name']		= 'Name';
$_['column_value']		= 'Wert';
$_['column_action']		= 'Aktion';
$_['error_permission']		= 'Keine Rechte für diese Aktion';
