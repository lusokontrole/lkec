<?php
$_['heading_title']		= 'Online';
$_['text_extension']		= 'Erweiterungen';
$_['text_success']		= 'Datensatz erfolgreich bearbeitet';
$_['text_list']		= 'Wer ist Online';
$_['text_filter']		= 'Filter';
$_['text_guest']		= 'Gast';
$_['column_ip']		= 'IP';
$_['column_customer']		= 'Kunde';
$_['column_url']		= 'Le. Seite angesehen';
$_['column_referer']		= 'Kommt Von';
$_['column_date_added']		= 'Erstellt';
$_['column_action']		= 'Aktion';
$_['entry_ip']		= 'IP';
$_['entry_customer']		= 'Kunde';
$_['entry_status']		= 'Status';
$_['entry_sort_order']		= 'Reihenfolge';
$_['error_permission']		= 'Keine Rechte für diese Aktion';
