<?php
$_['heading_title']		= 'Sichern &amp; Wiederherstellen';
$_['text_success']		= 'Datenbank erfolgreich importiert';
$_['entry_progress']		= 'Fortschritt<br><i style="color:coral">Hinweis: es können nur Daten, keine Strukturangaben wiederhergestellt werden!</i>';
$_['entry_export']		= 'Export<br><i style="color:coral">Achtung! Es werden nur Daten gesichert, keine Angaben zur Struktur!</i>';
$_['tab_backup']		= 'Sichern';
$_['tab_restore']		= 'Wiederherstellen';
$_['error_permission']		= 'Keine Rechte für diese Aktion';
$_['error_export']		= 'Es muss mindestens 1 Tabelle zur Sicherung ausgewählt werden';
$_['error_file']		= 'Datei konnte nicht gefunden werden';
