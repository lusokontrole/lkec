<?php
$_['heading_title']		= 'Berichte';
$_['text_success']		= 'Berichte erfolgreich bereinigt';
$_['text_list']		= 'Übersicht<br><small><b style="color:coral">Hinweis: darauf achten dass die Datei nicht zu groß wird, da dann das gesamte System langsamer wird!</b><br>Am besten regelmässig kontrollieren, sollten oft und viele Einträge sein, am besten einen Entwickler dazu befragen, z.B. <a href="https://osworx.net" target="_blank">OSWorX</a></small>';
$_['error_warning']		= 'Achtung: die Dateigröße von %s beträgt %s';
$_['error_permission']		= 'Keine Rechte zum bearbeiten';
