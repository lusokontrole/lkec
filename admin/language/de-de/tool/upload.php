<?php
$_['heading_title']		= 'Uploads';
$_['text_success']		= 'Einstellungen erfolgreich bearbeitet';
$_['text_list']		= 'Übersicht';
$_['text_filter']		= 'Filter';
$_['column_name']		= 'Uploadname';
$_['column_filename']		= 'Dateiname';
$_['column_date_added']		= 'Erstellt';
$_['column_action']		= 'Aktion';
$_['entry_name']		= 'Uploadname';
$_['entry_filename']		= 'Dateiname';
$_['entry_date_added']		= 'Erstellt';
$_['error_permission']		= 'Keine Rechte für diese Aktion';
$_['error_upload']		= 'Ungültiger Upload';
$_['error_file']		= 'Uploaddatei nicht gefunden';
