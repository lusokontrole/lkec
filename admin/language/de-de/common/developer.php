<?php
$_['heading_title']		= 'Entwicklereinstellungen';
$_['text_success']		= 'Einstellungen erfolgreich bearbeitet';
$_['text_theme']		= 'Vorlage';
$_['text_sass']		= 'SASS';
$_['text_cache']		= 'Cache %s erfolgreich geleert';
$_['column_component']		= 'Komponente';
$_['column_action']		= 'Aktion';
$_['entry_theme']		= 'Vorlage';
$_['entry_sass']		= 'SASS';
$_['entry_cache']		= 'Cache';
$_['button_on']		= 'An';
$_['button_off']		= 'Aus';
$_['error_permission']		= 'Keine Rechte für diese Aktion';
