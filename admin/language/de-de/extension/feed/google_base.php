<?php
$_['heading_title']		= 'Google Base';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Google Base Feed erfolgreich aktualisiert!';
$_['text_edit']		= 'Edit Google Base';
$_['text_import']		= 'To download the latest Google category list by <a href="https://support.google.com/merchants/answer/160081?hl=en" target="_blank" class="alert-link">clicking here</a> and choose taxonomy with numeric IDs in Plain Text (.txt) file. Upload via the green import button.';
$_['column_google_category']		= 'Google Category';
$_['column_category']		= 'Category';
$_['column_action']		= 'Action';
$_['entry_google_category']		= 'Google Category';
$_['entry_category']		= 'Category';
$_['entry_data_feed']		= 'URL Datenfeed:';
$_['entry_status']		= 'Status:';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Google Base zu ändern!';
$_['error_upload']		= 'File could not be uploaded!';
$_['error_filetype']		= 'Invalid file type!';
