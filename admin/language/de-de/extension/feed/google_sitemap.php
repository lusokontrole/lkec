<?php
$_['heading_title']		= 'Google Sitemap';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Google Sitemap Feed erfolgreich aktualisiert!';
$_['text_edit']		= 'Edit Google Sitemap';
$_['entry_status']		= 'Status:';
$_['entry_data_feed']		= 'URL Datenfeed:';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Google Sitemap zu ändern!';
