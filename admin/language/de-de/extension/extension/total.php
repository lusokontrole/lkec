<?php
$_['heading_title']		= 'Auftragssummen';
$_['text_success']		= 'Success: You have modified totals!';
$_['column_name']		= 'Bezeichnung';
$_['column_status']		= 'Status';
$_['column_sort_order']		= 'Reihenfolge';
$_['column_action']		= 'Aktion';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Auftragssummen zu ändern!';
