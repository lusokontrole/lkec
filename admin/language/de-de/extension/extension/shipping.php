<?php
$_['heading_title']		= 'Versand';
$_['text_success']		= 'Success: You have modified shipping!';
$_['column_name']		= 'Bezeichnung';
$_['column_status']		= 'Status';
$_['column_sort_order']		= 'Reihenfolge';
$_['column_action']		= 'Aktion';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Versandarten zu ändern!';
