<?php
$_['heading_title']		= 'Zahlung';
$_['text_success']		= 'Success: You have modified payments!';
$_['column_name']		= 'Bezeichnung';
$_['column_status']		= 'Status';
$_['column_sort_order']		= 'Reihenfolge';
$_['column_action']		= 'Aktion';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Zahlungen zu ändern!';
