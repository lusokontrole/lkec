<?php
$_['heading_title']		= 'Abholung';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Versandart Abholung wurde erfolgreich aktualisiert';
$_['text_edit']		= 'Edit Pickup From Store Shipping';
$_['entry_geo_zone']		= 'Geo Zone:';
$_['entry_status']		= 'Status:';
$_['entry_sort_order']		= 'Reihenfolge:';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um die Versandart Abholung zu ändern!';
