<?php
$_['heading_title']		= 'Versandkostenfreier Versand';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Versandkostenfreier Versand erfolgreich geändert!';
$_['text_edit']		= 'Edit Free Shipping';
$_['entry_total']		= 'Summe:<br /><span class="help">Erforderliche Bestellsumme für die versandkostenfreie Lieferung.</span>';
$_['entry_geo_zone']		= 'Geo Zone:';
$_['entry_status']		= 'Status:';
$_['entry_sort_order']		= 'Reihenfolge:';
$_['help_total']		= 'Sub-Total amount needed before the free shipping module becomes available.';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Versandkostenfreien Versand zu ändern!';
