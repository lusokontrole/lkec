<?php
$_['heading_title']		= 'Gewichtsabhängige Versandkosten';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Versand nach Gewicht erfolgreich geändert!';
$_['text_edit']		= 'Edit Weight Based Shipping';
$_['entry_rate']		= 'Gebühren:<br /><span class="help">Beispiel: 5:10.00,7:12.00 Gewicht:Kosten,Gewicht:Kosten, usw.</span>';
$_['entry_tax_class']		= 'Steuerklasse:';
$_['entry_geo_zone']		= 'Geo Zone:';
$_['entry_status']		= 'Status:';
$_['entry_sort_order']		= 'Reihenfolge:';
$_['help_rate']		= 'Example: 5:10.00,7:12.00 Weight:Cost,Weight:Cost, etc..';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Versand nach Gewicht zu ändern!';
