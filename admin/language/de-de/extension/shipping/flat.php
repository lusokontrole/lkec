<?php
$_['heading_title']		= 'Versandkostenpauschale';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Versandkostenpauschale erfolgreich geändert!';
$_['text_edit']		= 'Edit Flat Rate Shipping';
$_['entry_cost']		= 'Kosten:';
$_['entry_tax_class']		= 'Steuerklasse:';
$_['entry_geo_zone']		= 'Geo Zone:';
$_['entry_status']		= 'Status:';
$_['entry_sort_order']		= 'Reihenfolge:';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Versandkostenpauschale zu ändern!';
