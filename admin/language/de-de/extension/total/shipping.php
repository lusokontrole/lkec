<?php
$_['heading_title']		= 'Versand';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Versand erfolgreich geändert!';
$_['text_edit']		= 'Edit Shipping Total';
$_['entry_estimator']		= 'Frachtkalkulator:';
$_['entry_status']		= 'Status:';
$_['entry_sort_order']		= 'Reihenfolge:';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Versand zu ändern!';
