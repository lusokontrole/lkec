<?php
$_['heading_title']		= 'Aktionsgutschein';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Aktionsgutschein erfolgreich geändert!';
$_['text_edit']		= 'Edit Coupon';
$_['entry_status']		= 'Status:';
$_['entry_sort_order']		= 'Reihenfolge:';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Aktionsgutscheine zu ändern!';
