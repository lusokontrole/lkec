<?php
$_['heading_title']		= 'Mindermengenzuschlag';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Mindermengenzuschlag erfolgreich geändert!';
$_['text_edit']		= 'Edit Low Order Fee Total';
$_['entry_total']		= 'Auftragssumme:';
$_['entry_fee']		= 'Gebühr:';
$_['entry_tax_class']		= 'Steuerklasse:';
$_['entry_status']		= 'Status:';
$_['entry_sort_order']		= 'Reihenfolge:';
$_['help_total']		= 'The checkout total the order must reach before this order total is deactivated.';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Mindermengenzuschlag zu ändern!';
