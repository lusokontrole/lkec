<?php
$_['heading_title']		= 'Geschenkgutschein';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Geschenkgutschein erfolgreich geändert!';
$_['text_edit']		= 'Edit Gift Voucher Total';
$_['entry_status']		= 'Status:';
$_['entry_sort_order']		= 'Reihenfolge:';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Geschenkgutscheine zu bearbeiten!';
