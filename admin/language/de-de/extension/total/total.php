<?php
$_['heading_title']		= 'Gesamtsumme';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Gesamtsumme erfolgreich geändert!';
$_['text_edit']		= 'Edit Total Total';
$_['entry_status']		= 'Status:';
$_['entry_sort_order']		= 'Reihenfolge:';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Gesamtsumme zu ändern!';
