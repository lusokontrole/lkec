<?php
$_['heading_title']		= 'Paymate';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Paymate erfolgreich geändert!';
$_['text_edit']		= 'Edit Paymate';
$_['text_paymate']		= '<img src="view/image/payment/paymate.png" alt="Paymate" title="Paymate" style="border: 1px solid #EEEEEE;" />';
$_['entry_username']		= 'Paymate Benutzername:';
$_['entry_password']		= 'Passwort:<br /><span class="help">Bitte ein zufälliges Passwort auswählen. Dies stelle sicher, dass die Zahlungsinformation nach dem Senden an das Zahlungsgateway nicht verändert wurde.</span>';
$_['entry_test']		= 'Testmodus:';
$_['entry_total']		= 'Summe:<br /><span class="help">Der Warenkorb muss diese Summe beinhalten, damit dieses Zahlungsverfahren verfügbar ist.</span>';
$_['entry_order_status']		= 'Auftragsstatus:';
$_['entry_geo_zone']		= 'Geo Zone:';
$_['entry_status']		= 'Status:';
$_['entry_sort_order']		= 'Reihenfolge:';
$_['help_password']		= 'Just use some random password. This will be used to make sure the payment information is not interfered with after being sent to the payment gateway.';
$_['help_total']		= 'The checkout total the order must reach before this payment method becomes active.';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Paymate zu ändern!';
$_['error_username']		= 'Paymate Benutzername erforderlich!';
$_['error_password']		= 'Passwort erforderlich!';
