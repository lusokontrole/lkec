<?php
$_['heading_title']		= 'ohne Bezahlung';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Modul Ohne Bezahlung wurde erfolgreich geändert!';
$_['text_edit']		= 'Edit Free Checkout';
$_['entry_order_status']		= 'Auftragsstatus:';
$_['entry_status']		= 'Status:';
$_['entry_sort_order']		= 'Reihenfolge:';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Ohne Bezahlung zu ändern!';
