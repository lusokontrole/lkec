<?php
$_['heading_title']		= 'PayPal Standard';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: PayPal erfolgreich geändert!';
$_['text_edit']		= 'Edit PayPal Payments Standard';
$_['text_pp_standard']		= '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=W9TBB5DTD6QJW\');"><img src="view/image/payment/paypal.png" alt="PayPal" title="PayPal" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']		= 'Genehmigung';
$_['text_sale']		= 'Verkauf';
$_['entry_email']		= 'E-Mail:';
$_['entry_receiver_id']				 = 'Receiver ID';
$_['entry_test']		= 'Testmodus:';
$_['entry_transaction']		= 'Transaktionsmethode:';
$_['entry_debug']		= 'Debug Modus:<br /><span class="help">Protokolliert zusätzliche Informationen im Systemprotokoll.</span>';
$_['entry_total']		= 'Summe:<br /><span class="help">Der Warenkorb muss diese Summe beinhalten, damit dieses Zahlungsverfahren verfügbar ist.</span>';
$_['entry_canceled_reversal_status']		= 'Auftragsstatus Erstattung Abgebrochen:';
$_['entry_completed_status']		= 'Status Fertig:';
$_['entry_denied_status']		= 'Auftragsstatus Abgelehnt:';
$_['entry_expired_status']		= 'Status abgelaufen:';
$_['entry_failed_status']		= 'Auftragsstatus Fehlgeschlagen:';
$_['entry_pending_status']		= 'Auftragsstatus Ausstehend:';
$_['entry_processed_status']		= 'Status bearbeitet:';
$_['entry_refunded_status']		= 'Auftragsstatus Erstattet:';
$_['entry_reversed_status']		= 'Auftragsstatus Gutschrift:';
$_['entry_voided_status']		= 'Status ungültig:';
$_['entry_geo_zone']		= 'Geo Zone:';
$_['entry_status']		= 'Status:';
$_['entry_sort_order']		= 'Reihenfolge:';
$_['tab_general']		= 'General';
$_['tab_order_status']		= 'Order Status';
$_['help_test']		= 'Use the live or testing (sandbox) gateway server to process transactions?';
$_['help_debug']		= 'Logs additional information to the system log';
$_['help_total']		= 'The checkout total the order must reach before this payment method becomes active';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um PayPal zu ändern!';
$_['error_email']		= 'E-Mail erforderlich!';
