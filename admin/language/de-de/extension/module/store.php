<?php
$_['heading_title']		= 'Shop';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Modul Shop erfolgreich geändert!';
$_['text_edit']		= 'Edit Store Module';
$_['entry_admin']		= 'Nur Verwaltungsbenutzer:';
$_['entry_status']		= 'Status:';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um das Modul Shop zu ändern!';
