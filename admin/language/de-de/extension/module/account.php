<?php
$_['heading_title']		= 'Konto';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Modul Account erfolgreich geändert!';
$_['text_edit']		= 'Edit Account Module';
$_['entry_status']		= 'Status:';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um das Modul Account zu ändern!';
