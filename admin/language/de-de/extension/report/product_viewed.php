<?php
$_['heading_title']		= 'Gesehene Produkte';
$_['text_extension']		= 'Extensions';
$_['text_edit']		= 'Edit Products Viewed Report';
$_['text_success']		= 'Erfolgreich: Report wurde erfolgreich zurückgesetzt!';
$_['column_name']		= 'Bezeichnung';
$_['column_model']		= 'Modell';
$_['column_viewed']		= 'Angesehen';
$_['column_percent']		= 'Prozent';
$_['entry_status']		= 'Status';
$_['entry_sort_order']		= 'Sort Order';
$_['error_permission']		= 'Warning: You do not have permission to modify products viewed report!';
