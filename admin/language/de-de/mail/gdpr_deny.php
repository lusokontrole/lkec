<?php
$_['text_subject']		= '%s - Anfrage nach DSGVO abgelehnt';
$_['text_export']		= 'Account Export Data Request';
$_['text_remove']		= 'Account Deletion Request';
$_['text_hello']		= 'Guten Tag <strong>%s</strong>';
$_['text_user']		= 'User';
$_['text_contact']		= 'Der Antrag zur Löscung des KOntos wurde abgelehnt, für weitere Informationen sind wir hier erreichbar:';
$_['text_thanks']		= 'Vielen Dank,';
$_['button_contact']		= 'Uns kontaktieren';
