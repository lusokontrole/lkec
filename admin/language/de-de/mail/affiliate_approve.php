<?php
$_['text_subject']		= '%s - das Partnerkonto wurde aktiviert.';
$_['text_welcome']		= 'Willkommen und Danke für die Registrierung auf %s';
$_['text_login']		= 'Das Konto wurde freigeschaltet, Anmeldung erfolgt mit Emailadresse und Passwort unter folgender Adresse:';
$_['text_service']		= 'Upon logging in, you will be able to generate tracking codes, track commission payments and edit your account information.';
$_['text_thanks']		= 'Vielen Dank,';
$_['button_login']		= 'Login';
