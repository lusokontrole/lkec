<?php
$_['heading_title']		= 'Seite nicht gefunden';
$_['text_not_found']		= 'Die angeforderte Seite konnte leider nicht gefunden werden.<br />Sollte das Problem weiterhin bestehen, bitte die Shopverwaltung davon informieren (siehe Kontakt) - vielen Dank.';
