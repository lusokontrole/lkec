<?php
$_['heading_title']		= 'Retourengrund';
$_['text_success']		= 'Datensatz erfolgreich bearbeitet';
$_['text_list']		= 'Übersicht';
$_['text_add']		= 'Neu';
$_['text_edit']		= 'Bearbeiten';
$_['column_name']		= 'Name';
$_['column_action']		= 'Aktion';
$_['entry_name']		= 'Name';
$_['error_permission']		= 'Keine Rechte für diese Aktion';
$_['error_name']		= 'Name muss zwischen 3 und 128 Zeichen lang sein';
$_['error_return']		= 'Retourengrund kann nicht gelöscht werden da er %s Retoure(n) zugeordnet ist';
