<?php
$_['heading_title']		= 'Upload';
$_['text_success']		= 'Upload modificati con successo!';
$_['text_list']		= 'Lista Upload';
$_['text_filter']		= 'Filter';
$_['column_name']		= 'Nome Upload';
$_['column_filename']		= 'Nome File';
$_['column_date_added']		= 'Data Aggiunta';
$_['column_action']		= 'Azione';
$_['entry_name']		= 'Nome Upload';
$_['entry_filename']		= 'Nome File';
$_['entry_date_added']		= 'Data Aggiunta';
$_['error_permission']		= 'Attenzione: non si hanno i permessi per modificare gli upload!';
$_['error_upload']		= 'Invalid upload!';
$_['error_file']		= 'Upload file is not found!';
