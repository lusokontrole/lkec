<?php
$_['heading_title']		= 'Gruppi Attributo';
$_['text_success']		= 'Gruppi attributo modificato con successo!';
$_['text_list']		= 'Lista Gruppo Attributo';
$_['text_add']		= 'Aggiungi Lista Gruppo Attributo';
$_['text_edit']		= 'Modifica Lista Gruppo Attributo';
$_['column_name']		= 'Nome Gruppo Attributo';
$_['column_sort_order']		= 'Ordinamento';
$_['column_action']		= 'Azione';
$_['entry_name']		= 'Nome Gruppo Attributo';
$_['entry_sort_order']		= 'Ordinamento';
$_['error_warning']		= 'Warning: Please check the form carefully for errors!';
$_['error_permission']		= 'Attenzion: Non si hanno i permessi per modificare i gruppi attributo!';
$_['error_name']		= 'Il Nome Gruppo Attributo deve essere tra 3 e 64 caratteri!';
$_['error_attribute']		= 'Attenzione: Questo gruppo non pu&ograve; essere eliminato in quanto assegnato a %s attributi!';
$_['error_product']		= 'Attenzione: Questo gruppo non pu&ograve; essere eliminato in quanto assegnato a %s prodotti!';
