<?php
$_['heading_title']		= 'Motivazioni Resi (RMA)';
$_['text_success']		= 'Motivazioni Resi (RMA) modificato con successo!';
$_['text_list']		= 'Lista Motivazione Resi (RMA)';
$_['text_add']		= 'Aggiungi Motivazione Resi (RMA)';
$_['text_edit']		= 'Modifica Motivazione Resi (RMA)';
$_['column_name']		= 'Nome Motivazione Resi (RMA)';
$_['column_action']		= 'Azione';
$_['entry_name']		= 'Nome Motivazione Resi (RMA)';
$_['error_permission']		= 'Attenzione: Non si hanno i permessi per modificare le motivazioni sulle Resi!';
$_['error_name']		= 'Nome Motivazione deve essere lungo dai 3 ai 32 caratteri!';
$_['error_return']		= 'Attenzione: Questa motivazione non pu&ograve; essere eliminata perch&eacute; &egrave; assegnata a %s prodotti restituiti!';
