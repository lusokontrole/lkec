<?php
$_['heading_title']		= 'Voucher Regalo';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Hai modificato il totale dei voucher regalo!';
$_['text_edit']		= 'Edit Gift Voucher Total';
$_['entry_status']		= 'Stato:';
$_['entry_sort_order']		= 'Ordinamento:';
$_['error_permission']		= 'Attenzione: Non hai i permessi necessari per modificare questa opzione!';
