<?php
$_['heading_title']		= 'Supplemento per ordine piccolo';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Supplemento per ordine piccolo modificata con successo!';
$_['text_edit']		= 'Edit Low Order Fee Total';
$_['entry_total']		= 'Totale ordine:';
$_['entry_fee']		= 'Supplemento:';
$_['entry_tax_class']		= 'Tax Class';
$_['entry_status']		= 'Stato:';
$_['entry_sort_order']		= 'Ordinamento:';
$_['help_total']		= 'The checkout total the order must reach before this order total is deactivated.';
$_['error_permission']		= 'Attenzione: non hai i permessi per modificare il supplemento per ordine piccolo!';
