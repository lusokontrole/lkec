<?php
$_['heading_title']		= 'Punti';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Hai modificato Punti con successo!';
$_['text_edit']		= 'Edit Reward Points Total';
$_['entry_status']		= 'Stato:';
$_['entry_sort_order']		= 'Ordinamento:';
$_['error_permission']		= 'Attenzione: Non hai il permesso di modificare Punti!';
