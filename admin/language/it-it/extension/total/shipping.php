<?php
$_['heading_title']		= 'Spedizione';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Spedizioni modificate con successo!';
$_['text_edit']		= 'Edit Shipping Total';
$_['entry_estimator']		= 'Stima Spedizione:';
$_['entry_status']		= 'Stato:';
$_['entry_sort_order']		= 'Ordinamento:';
$_['error_permission']		= 'Attenzione: non hai i permessi per modificare la spedizione nel totale ordine!';
