<?php
$_['heading_title']		= 'Rapporto Coupon';
$_['text_extension']		= 'Extensions';
$_['text_edit']		= 'Edit Coupons Report';
$_['text_success']		= 'Success: You have modified coupon report!';
$_['text_filter']		= 'Filter';
$_['column_name']		= 'Nome Coupon';
$_['column_code']		= 'Codice';
$_['column_orders']		= 'Ordini';
$_['column_total']		= 'Totale';
$_['column_action']		= 'Azione';
$_['entry_date_start']		= 'Data Inizio';
$_['entry_date_end']		= 'Data Fine';
$_['entry_status']		= 'Status';
$_['entry_sort_order']		= 'Sort Order';
$_['error_permission']		= 'Warning: You do not have permission to modify coupon report!';
