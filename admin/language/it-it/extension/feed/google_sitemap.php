<?php
$_['heading_title']		= 'Google Sitemap';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Hai modificato Google Sitemap con successo!';
$_['text_edit']		= 'Edit Google Sitemap';
$_['entry_status']		= 'Stato:';
$_['entry_data_feed']		= 'Dati Feed Url:';
$_['error_permission']		= 'Attenzione: Non hai i permessi per modificare Google Sitemap!';
