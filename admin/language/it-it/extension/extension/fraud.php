<?php
$_['heading_title']		= 'Anti-Fraud';
$_['text_success']		= 'Success: You have modified anti-fraud!';
$_['column_name']		= 'Anti-Fraud Name';
$_['column_status']		= 'Status';
$_['column_action']		= 'Action';
$_['error_permission']		= 'Warning: You do not have permission to modify anti-fraud!';
