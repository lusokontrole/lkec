<?php
$_['heading_title']		= 'Analytics';
$_['text_success']		= 'Success: You have modified analytics!';
$_['column_name']		= 'Analytics Name';
$_['column_status']		= 'Status';
$_['column_action']		= 'Action';
$_['error_permission']		= 'Warning: You do not have permission to modify analytics!';
