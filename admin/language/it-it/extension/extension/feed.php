<?php
$_['heading_title']		= 'Feed prodotti';
$_['text_success']		= 'Success: You have modified feeds!';
$_['column_name']		= 'Nome feed prodotto';
$_['column_status']		= 'Stato';
$_['column_action']		= 'Azione';
$_['error_permission']		= 'Attenzione: non hai i permessi per modificare i feed!';
