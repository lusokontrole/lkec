<?php
$_['heading_title']		= 'Dashboard';
$_['text_success']		= 'Success: You have modified dashboards!';
$_['column_name']		= 'Dashboard Name';
$_['column_width']		= 'Width';
$_['column_status']		= 'Status';
$_['column_sort_order']		= 'Sort Order';
$_['column_action']		= 'Action';
$_['error_permission']		= 'Warning: You do not have permission to modify dashboards!';
