<?php
$_['heading_title']		= 'Bonifico Bancario';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Hai modificato con successo i dettagli per il bonifico bancario!';
$_['text_edit']		= 'Edit Bank Transfer';
$_['entry_bank']		= 'Istruzioni bonifico bancario:';
$_['entry_total']		= 'Totale:<br /><span class="help">Il totale che deve essere raggiunto per abilitare questo metodo di pagamento.</span>';
$_['entry_order_status']		= 'Stato Ordine:';
$_['entry_geo_zone']		= 'Zona Geografica:';
$_['entry_status']		= 'Stato:';
$_['entry_sort_order']		= 'Ordina per:';
$_['help_total']		= 'The checkout total the order must reach before this payment method becomes active.';
$_['error_permission']		= 'Attenzione: Non hai i permessi necessari per modificare il modulo bonifico bancario!';
$_['error_bank']		= 'Istruzioni per bonifico bancario richieste!';
