<?php
$_['heading_title']		= 'Banner';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Hai modificato il modulo banner con successo!';
$_['text_edit']		= 'Edit Banner Module';
$_['entry_name']		= 'Module Name';
$_['entry_banner']		= 'Banner:';
$_['entry_width']		= 'Width';
$_['entry_height']		= 'Height';
$_['entry_status']		= 'Stato:';
$_['error_permission']		= 'Attenzione: non hai il permesso di modificare il modulo banner!';
$_['error_name']		= 'Module Name must be between 3 and 64 characters!';
$_['error_width']		= 'Width required!';
$_['error_height']		= 'Height required!';
