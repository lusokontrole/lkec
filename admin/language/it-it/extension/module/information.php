<?php
$_['heading_title']		= 'Informazioni';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Hai modificato il modulo informazioni con successo!';
$_['text_edit']		= 'Edit Information Module';
$_['entry_status']		= 'Stato:';
$_['error_permission']		= 'Attenzione: non hai il permesso di modificare il modulo informazioni!';
