<?php
$_['heading_title']		= 'Spedizione gratuita';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Hai modificato con successo il modulo Spedizione gratuita!';
$_['text_edit']		= 'Edit Free Shipping';
$_['entry_total']		= 'Totale:<br /><span class="help">Importo sopra il quale viene inserita la spedizione gratuita. Attenzione: sub-totale necessario affinch&egrave; il modulo spedizione gratuita sia disponibile.</span>';
$_['entry_geo_zone']		= 'Zona geografica:';
$_['entry_status']		= 'Stato:';
$_['entry_sort_order']		= 'Ordina per:';
$_['help_total']		= 'Sub-Total amount needed before the free shipping module becomes available.';
$_['error_permission']		= 'Attenzione: non hai i permessi per modificare il modulo Spedizione gratuita!';
