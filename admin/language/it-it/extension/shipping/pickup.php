<?php
$_['heading_title']		= 'Ritiro in sede';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Hai modificato con successo il modulo Ritiro in negozio!';
$_['text_edit']		= 'Edit Pickup From Store Shipping';
$_['entry_geo_zone']		= 'Zona geografica:';
$_['entry_status']		= 'Stato:';
$_['entry_sort_order']		= 'Ordinamento:';
$_['error_permission']		= 'Attenzione: non hai i permessi necessari per modificare il modulo Ritiro in negozio!';
