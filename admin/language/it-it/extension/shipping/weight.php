<?php
$_['heading_title']		= 'Spedizione basata sul peso';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Hai modificato con successo il modulo spezione basata sul peso!';
$_['text_edit']		= 'Edit Weight Based Shipping';
$_['entry_rate']		= 'Peso:Costo:<br /><span class="help">Esempio: 5:10.00,7:12.00 Peso:Costo,Peso:Costo, etc..</span>';
$_['entry_tax_class']		= 'Tax Class';
$_['entry_geo_zone']		= 'Zona geografica:';
$_['entry_status']		= 'Stato:';
$_['entry_sort_order']		= 'Ordina per:';
$_['help_rate']		= 'Example: 5:10.00,7:12.00 Weight:Cost,Weight:Cost, etc..';
$_['error_permission']		= 'Attenzione: non hai i permessi per modificare il modulo spedizione in base al peso!';
