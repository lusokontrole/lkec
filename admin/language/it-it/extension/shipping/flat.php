<?php
$_['heading_title']		= 'Costo fisso';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Spedizione a costo fisso modificata correttamente!';
$_['text_edit']		= 'Edit Flat Rate Shipping';
$_['entry_cost']		= 'Costo:';
$_['entry_tax_class']		= 'Tax Class';
$_['entry_geo_zone']		= 'Zona geografica:';
$_['entry_status']		= 'Stato:';
$_['entry_sort_order']		= 'Ordinamento:';
$_['error_permission']		= 'Attenzione: non hai i permessi per modificare la spedizione a costo fisso!';
