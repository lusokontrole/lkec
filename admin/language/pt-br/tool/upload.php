<?php
$_['heading_title']		= 'Uploads';
$_['text_success']		= 'Upload modificado com sucesso!';
$_['text_list']		= 'Listando uploads';
$_['text_filter']		= 'Filter';
$_['column_name']		= 'Upload';
$_['column_filename']		= 'Arquivo';
$_['column_date_added']		= 'Enviado';
$_['column_action']		= 'Ação';
$_['entry_name']		= 'Upload';
$_['entry_filename']		= 'Arquivo';
$_['entry_date_added']		= 'Enviado';
$_['error_permission']		= 'Atenção: Você não tem permissão para gerenciar os uploads!';
$_['error_upload']		= 'Upload inválido!';
$_['error_file']		= 'O arquivo não foi encontrado!';
