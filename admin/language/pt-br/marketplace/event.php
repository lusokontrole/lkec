<?php
$_['heading_title']		= 'Eventos';
$_['text_success']		= 'Eventos modificado com sucesso!';
$_['text_list']		= 'Listando eventos';
$_['text_event']		= 'Eventos são utilizados por extensões para substituir funcionalidades que são padrão de sua loja. Se você tiver problemas, você pode desativar ou ativar os eventos aqui.';
$_['text_info']		= 'Informações sobre o evento';
$_['text_trigger']		= 'Gatilho';
$_['text_action']		= 'Ação';
$_['column_code']		= 'Evento';
$_['column_status']		= 'Situação';
$_['column_sort_order']		= 'Ordem';
$_['column_action']		= 'Ação';
$_['error_permission']		= 'Atenção: Você não tem permissão para modificar os eventos!';
