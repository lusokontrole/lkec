<?php
$_['heading_title']		= 'Administração';
$_['text_heading']		= 'Administração';
$_['text_login']		= 'Digite seus dados de acesso.';
$_['text_forgotten']		= 'Esqueceu sua senha?';
$_['entry_username']		= 'Usuário';
$_['entry_password']		= 'Senha';
$_['button_login']		= 'Acessar';
$_['error_login']		= 'Os dados de acesso não são válidos.';
$_['error_token']		= 'O token de sessão é inválido. Tente acessar novamente.';
$_['error_attempts']		= 'Warning: Your account has exceeded allowed number of login attempts. Please try again in 1 hour or reset password.';
