<?php
$_['heading_title']		= 'Temas dos vales presentes';
$_['text_success']		= 'Tema modificado com sucesso!';
$_['text_list']		= 'Listando temas dos vales presentes';
$_['text_add']		= 'Novo tema';
$_['text_edit']		= 'Editando tema';
$_['column_name']		= 'Nome do tema';
$_['column_action']		= 'Ação';
$_['entry_name']		= 'Nome do tema';
$_['entry_description']		= 'Descrição';
$_['entry_image']		= 'Imagem';
$_['error_permission']		= 'Atenção: Você não tem permissão para modificar temas dos vales presentes!';
$_['error_name']		= 'O nome do tema deve ter entre 3 e 32 caracteres!';
$_['error_image']		= 'A imagem é obrigatória!';
$_['error_voucher']		= 'Atenção: Este tema não pode ser excluído, pois está vinculado a %s vales presentes!';
