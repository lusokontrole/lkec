<?php
$_['heading_title']		= 'Estatísticas';
$_['text_success']		= 'Estatística modificada com sucesso!';
$_['text_list']		= 'Listando estatísticas';
$_['text_order_sale']		= 'Pedidos';
$_['text_order_processing']		= 'Pedidos sendo processados';
$_['text_order_complete']		= 'Pedidos finalizados';
$_['text_order_other']		= 'Pedidos em outras situações';
$_['text_return']		= 'Returns';
$_['text_customer']		= 'Clientes aguardando aprovação';
$_['text_affiliate']		= 'Afiliados aguardando aprovação';
$_['text_product']		= 'Produtos sem estoque';
$_['text_review']		= 'Comentários pendentes';
$_['column_name']		= 'Estatística';
$_['column_value']		= 'Valor';
$_['column_action']		= 'Ação';
$_['error_permission']		= 'Atenção: Você não tem permissão para atualizar as estatísticas!';
