<?php
$_['heading_title']		= 'Grupos de atributos';
$_['text_success']		= 'Grupo de atributos modificado com sucesso!';
$_['text_list']		= 'Listando grupos de atributos';
$_['text_add']		= 'Novo grupo de atributos';
$_['text_edit']		= 'Editando grupo de atributos';
$_['column_name']		= 'Grupo de atributos';
$_['column_sort_order']		= 'Posição';
$_['column_action']		= 'Ação';
$_['entry_name']		= 'Grupo de atributos';
$_['entry_sort_order']		= 'Posição';
$_['error_warning']		= 'Warning: Please check the form carefully for errors!';
$_['error_permission']		= 'Atenção: Você não tem permissão para modificar os grupos de atributos!';
$_['error_name']		= 'Grupo de atributos deve ter entre 1 e 64 caracteres!';
$_['error_attribute']		= 'Atenção: Este grupo de atributos não pode ser excluído, pois está vinculado a %s atributos!';
$_['error_product']		= 'Atenção: Este grupo de atributos não pode ser excluído, pois está vinculado a %s produtos!';
