<?php
$_['heading_title']		= 'Australia Post';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Frete Australia Post modificado com sucesso!';
$_['text_edit']		= 'Configurações do frete Australia Post';
$_['entry_api']		= 'API Key';
$_['entry_postcode']		= 'CEP';
$_['entry_weight_class']		= 'Unidade de peso';
$_['entry_tax_class']		= 'Grupo de impostos';
$_['entry_geo_zone']		= 'Região geográfica';
$_['entry_status']		= 'Situação';
$_['entry_sort_order']		= 'Posição';
$_['help_weight_class']		= 'Defina para gramas.';
$_['error_permission']		= 'Atenção: Você não tem permissão para modificar o frete Australia Post!';
$_['error_postcode']		= 'O CEP deve ter 4 dígitos!';
