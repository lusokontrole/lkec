<?php
$_['heading_title']		= 'Expedição grátis';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Expedição grátis modificado com sucesso!';
$_['text_edit']		= 'Configurações da expedição grátis';
$_['entry_total']		= 'Sub-total mínimo';
$_['entry_geo_zone']		= 'Região geográfica';
$_['entry_status']		= 'Estado';
$_['entry_sort_order']		= 'Posição';
$_['help_total']		= 'O valor mínimo que o sub-total do pedido deve alcançar para que a expedição grátis seja oferecida ao cliente.';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar a expedição grátis!';
