<?php
$_['heading_title']		= 'Últimos pedidos';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Success: You have modified dashboard recent orders!';
$_['text_edit']		= 'Edit Dashboard Recent Orders';
$_['column_order_id']		= 'Pedido Nº';
$_['column_customer']		= 'Cliente';
$_['column_status']		= 'Estado';
$_['column_total']		= 'Total';
$_['column_date_added']		= 'Data';
$_['column_action']		= 'Ação';
$_['entry_status']		= 'Status';
$_['entry_sort_order']		= 'Sort Order';
$_['entry_width']		= 'Width';
$_['error_permission']		= 'Warning: You do not have permission to modify dashboard recent orders!';
