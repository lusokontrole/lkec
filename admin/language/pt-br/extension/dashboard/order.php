<?php
$_['heading_title']		= 'Total de pedidos';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Success: You have modified dashboard orders!';
$_['text_edit']		= 'Edit Dashboard Orders';
$_['text_view']		= 'Ver mais...';
$_['entry_status']		= 'Status';
$_['entry_sort_order']		= 'Sort Order';
$_['entry_width']		= 'Width';
$_['error_permission']		= 'Warning: You do not have permission to modify dashboard orders!';
