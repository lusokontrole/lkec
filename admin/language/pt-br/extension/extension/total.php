<?php
$_['heading_title']		= 'Totais de pedido';
$_['text_success']		= 'Total de pedido modificado com sucesso!';
$_['column_name']		= 'Total';
$_['column_status']		= 'Estado';
$_['column_sort_order']		= 'Posição';
$_['column_action']		= 'Ação';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar o total de pedido!';
