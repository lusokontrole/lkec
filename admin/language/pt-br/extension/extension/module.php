<?php
$_['heading_title']		= 'Módulos';
$_['text_success']		= 'Módulo modificado com sucesso!';
$_['text_layout']		= 'Depois de instalar e configurar o módulo, para o módulo ser exibido deve adicioná-lo a um layout <a href="%s" class="alert-link">clicando aqui</a> !';
$_['column_name']		= 'Módulo';
$_['column_status']		= 'Status';
$_['column_action']		= 'Ação';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar os módulos!';
$_['error_name']		= 'O nome do módulo deve ter entre 3 e 64 caracteres!';
$_['error_code_name']		= 'Module Code must be between 3 and 32 characters!';
$_['error_code']		= 'É necessário selecionar um módulo!';
