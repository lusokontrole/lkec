<?php
$_['heading_title']		= 'Cupão';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'CupoCupãom modificado com sucesso!';
$_['text_edit']		= 'Configurações do rebatimento de cupões';
$_['entry_status']		= 'Estado';
$_['entry_sort_order']		= 'Posição';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar o cupão!';
