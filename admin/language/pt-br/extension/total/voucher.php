<?php
$_['heading_title']		= 'Cheque brinde';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Cheque brinde modificado com sucesso!';
$_['text_edit']		= 'Configurações do cheque brinde';
$_['entry_status']		= 'Estado';
$_['entry_sort_order']		= 'Posição';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar o cheque brinde!';
