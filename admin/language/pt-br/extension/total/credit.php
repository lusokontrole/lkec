<?php
$_['heading_title']		= 'Crédito';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Crédito modificado com sucesso!';
$_['text_edit']		= 'Configurações do Crédito';
$_['entry_status']		= 'Estado';
$_['entry_sort_order']		= 'Posição';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar o crédito!';
