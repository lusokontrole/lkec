<?php
$_['heading_title']		= 'Menu de categorias';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Módulo menu de categorias modificado com sucesso!';
$_['text_edit']		= 'Configurações do módulo menu de categorias';
$_['entry_status']		= 'Estado';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar o módulo menu de categorias!';
