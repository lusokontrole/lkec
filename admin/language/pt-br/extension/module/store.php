<?php
$_['heading_title']		= 'Menu de lojas online';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Módulo menu de lojas online modificado com sucesso!';
$_['text_edit']		= 'Configurações do módulo menu de lojas online';
$_['entry_admin']		= 'Restrito a administradores';
$_['entry_status']		= 'Estado';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar o módulo menu de lojas online!';
