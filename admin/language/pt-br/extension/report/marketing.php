<?php
$_['heading_title']		= 'Relatório de marketing';
$_['text_extension']		= 'Extensions';
$_['text_edit']		= 'Edit Marketing Report';
$_['text_success']		= 'Success: You have modified marketing report!';
$_['text_filter']		= 'Filter';
$_['text_all_status']		= 'Todos';
$_['column_campaign']		= 'Campanha';
$_['column_code']		= 'Código';
$_['column_clicks']		= 'Cliques';
$_['column_orders']		= 'Pedidos';
$_['column_total']		= 'Total';
$_['entry_date_start']		= 'Data inicial';
$_['entry_date_end']		= 'Data final';
$_['entry_order_status']		= 'Order Status';
$_['entry_status']		= 'Estado do pedido';
$_['entry_sort_order']		= 'Sort Order';
$_['error_permission']		= 'Warning: You do not have permission to modify marketing report!';
