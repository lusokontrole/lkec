# Souk

## Overview

Minimum PHP Version: (8.0) 
Git Repository: https://codeberg.org/khadija-agency/souk)

Souk is a free open source ecommerce platform for online merchants. Souk provides a professional and reliable foundation from which to build a successful online store.


## How to install

Please read the [installation instructions](INSTALL.md) included in the repository or download file.


## How to upgrade from previous versions

Please read the [upgrading instructions](UPGRADE.md) included in the repository or download file.

## Reporting a bug

Read the instructions below before you create a bug report.

 1. Search the [Souk forum](https://spaces.khadija.agency/), ask the community if they have seen the bug or know how to fix it.
 2. Check all open and closed issues on the [Bug tracker](https://codeberg.org/khadija-agency/souk/issues).
 3. If your bug is related to the Souk core code then please create a bug report on GitHub.
 4. READ the [changelog for the master branch](https://codeberg.org/khadija-agency/souk/blob/master/CHANGELOG.md)
 5. Use [Google](https://www.google.com) to search for your issue.
 6. Make sure that your bug/issue is not related to your hosting environment.

If you are not sure about your issue, it is always best to ask the community on our [bug forum thread](https://spaces.khadija.agency/)

**Important!**
- If your bug report is not related to the core code (such as a 3rd party module or your server configuration) then the issue will be closed without a reason. You must contact the extension developer, use the forum or find a commercial partner to resolve a 3rd party code issue.
- If you would like to report a serious security bug please PM an Souk moderator/administrator on the forum. Please do not report concept/ideas/unproven security flaws - all security reports are taken seriously but you must include the EXACT details steps to reproduce it. Please DO NOT post security flaws in a public location.

## How to contribute

Fork the repository, edit and submit a pull request

Please be very clear on your commit messages and pull request, empty pull request messages may be rejected without reason.

## Versioning

The version is broken down into 3 points e.g 1.2.3 We use MAJOR.MINOR.PATCH to describe the version numbers.

A MAJOR is very rare, it would only be considered if the source was effectively re-written or a clean break was desired for other reasons. This increment would likely break most 3rd party modules.

A MINOR is when there are significant changes that affect core structures. New extensions or features are added (such as a payment gateway, shipping module etc) for future patching. Updating a MINOR version is at a low risk of breaking 3rd party modules.

A PATCH version is when a fix is added, it should be considered safe to update patch versions e.g 1.2.3 to 1.2.4

## Releases

MAJOR or bigger releases (ones that contain many core changes, features and fixes) an extended period will be considered following an announced branch r-MAJOR.0.0

MINOR team announce to developers 1 week prior to public release with a branch r-MAJOR.MINOR.0, this is to allow for testing of their own modules for compatibility.  

PATCH versions (which are considered safe to update with) may have a significantly reduced developer release period. Bug fixes or changes that don't break API, modules etc. r-MAJOR.MINOR.PATCH

Integration requests should be made on next release branch; r-MAJOR.MINOR.PATCH (PATCH branch). The master branch will always contain current stable release. The develop branch source code contains next MAJOR.

If a bug is found in an announced developer release that is significant (such as a major feature is broken) then the release will be pulled. A patch version will be issued to replace it, depending on the severity of the patch an extended testing period may be announced. If the developer release version was never made public then the preceding patch version tag will be removed.

To receive developer notifications about release information, sign up to the newsletter on the [Souk website](https://khadija.agency/newsletter) - located in the footer. Then choose the developer news option.


## License

[GNU General Public License version 3 (GPLv3)](https://codeberg.org/khadija-agency/souk/blob/master/LICENSE.md)

## Links

- [Souk homepage](https://khadija.agency/)
- [Souk forums](https://spaces.khadija.agency/)
- [Souk blog](https://spaces.khadija.agency/u/souk/)
- [How to documents](https://wiki.khadija.agency/souk)
- [Newsletter](https://khadija.agency/newsletter)
