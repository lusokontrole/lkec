<?php
class ModelInstallInstall extends Model {
	//private $create_files = true;
	private $create_files = false;

	private function create_db_files($db_prefix){
	
		// Data
		$lines = file(DIR_APPLICATION . 'souk.sql', FILE_IGNORE_NEW_LINES);
		if ($lines) {
			$sql = '';

			$start = false;

			foreach ($lines as $line) {
				if (substr($line, 0, 12) == 'INSERT INTO ') {
					$table_name = explode(" ", $line)[2];
					$table_name = trim($table_name, '`"');
					$table_name = str_replace("lk_", "", $table_name);

					$line		= str_replace("lk_", $db_prefix, $line);

					$sql 		= '';
					$start 		= true;
					$path 		= DIR_APPLICATION . 'db/data/';
				}
				if (substr($line, 0, 13) == 'CREATE TABLE ') {
					$table_name = explode(" ", $line)[2];
					$table_name = trim($table_name, '`"');
					$table_name = str_replace("lk_", "", $table_name);

					$line		= str_replace("lk_", $db_prefix, $line);

					$sql 		= '';
					$start 		= true;
					$path 		= DIR_APPLICATION . 'db/tables/';
				}


				if ($start) {
					$sql .= $line . "\n";

					if (substr($line, -2) == ');' OR substr($line, -1) == ';') {
						file_put_contents($path.$table_name.'.sql', $sql);
						$start = false;
					}

				}
			}
		}

	}

	public function create_tables($pdo, $db_prefix){
		$tables = array();
		$sql = "";

		// get list of current tables
		$table_list = $pdo->query("SHOW TABLES")->fetchAll(\PDO::FETCH_COLUMN);

		// start loading table structure files
		$files = glob(DIR_APPLICATION . "db/tables/*.sql");
		foreach($files as $file) {
			$table_name = basename($file, ".sql");
			$tables[$table_name]['create_file'] = $file;

			// check if table exists
			if (in_array($db_prefix . $table_name, $table_list, true)){
				$tables[$table_name]['drop'] = $pdo->prepare("DROP TABLE `" . $db_prefix . $table_name . "`");
			}

			// load sql from file
			$sql = file_get_contents($file);

			// set correct db_prefix
			$sql = str_replace("CREATE TABLE `lk_", "CREATE TABLE `" . $db_prefix, $sql);
	
			$tables[$table_name]['create'] = $pdo->prepare($sql);
			unset($sql);
		}

		foreach($tables as $table_name => $table) {
			if(isset($table['drop'])) {
				$table['drop']->execute();
			}
			$table['create']->execute();
		}		
		return $tables;
	}

	public function insert_data($pdo, $db_prefix) {

		// setup application	
		$files = glob(DIR_APPLICATION . "db/data/*.sql");
		foreach($files as $file) {
			$table_name = basename($file, ".sql");
			$tables[$table_name]['insert_file'] = $file;

			$sql = file_get_contents($file);

			// set correct db_prefix
			$sql = str_replace("INSERT INTO `lk_", "INSERT INTO `" . $db_prefix, $sql);
			try {
				$tables[$table_name]['insert'] = $pdo->prepare($sql);
				unset($sql);

			} catch( PDOException $Exception ) {
				echo "error on table ".$table_name." data: ".$Exception->getMessage()." ".$Exception->getCode();
				exit();
			}
		}

		foreach($tables as $table_name => $table) {
			try {
			$table['insert']->execute();
			} catch( PDOException $Exception ) {
				echo "error on table ".$table_name." data: ".$Exception->getMessage()." ".$Exception->getCode();
				exit();
			}
		}
		return $tables;
	}

	public function database(array $data): void {

		// create sql files for each table from
		// single sql dump file. Tables structure 
		// in tables folder and data in data folder
		if($this->create_files == true){
			$this->create_db_files($data['db_prefix']);
		}

		$pdo = new DBPDO($data['db_driver'], html_entity_decode($data['db_hostname'], ENT_QUOTES, 'UTF-8'), html_entity_decode($data['db_username'], ENT_QUOTES, 'UTF-8'), html_entity_decode($data['db_password'], ENT_QUOTES, 'UTF-8'), html_entity_decode($data['db_database'], ENT_QUOTES, 'UTF-8'), $data['db_port']);

		// create tables
		$this->create_tables($pdo, $data['db_prefix']);

		// insert data
		$this->insert_data($pdo, $data['db_prefix']);

		// setup application
		$db_prefix = $data['db_prefix'];
		$user_delete = $pdo->prepare("DELETE FROM " . $db_prefix . "user WHERE user_id = '1'");
		$user_insert = $pdo->prepare("INSERT INTO " . $db_prefix . "user SET 
			user_id 	= '1', 
			user_group_id 	= '1', 
			username 	= :username,
			password 	= :password,
			firstname 	= 'System', 
			lastname 	= 'Admin',
			email 		= :email,
			status 		= '1',
			image		= 'profile.png',
			code		= '',
			ip			= '',
			date_added 	= NOW()");

		$email_delete = $pdo->prepare("DELETE FROM " . $db_prefix . "setting WHERE \"key\" = 'config_email'");

		$email_insert = $pdo->prepare("INSERT INTO " . $db_prefix . "setting VALUES 
			(NULL, 0, 'config', 'config_email', ?, 0)");

		$encrypt_delete = $pdo->prepare("DELETE FROM " . $db_prefix . "setting WHERE \"key\"= 'config_encryption'");

		$encrypt_insert = $pdo->prepare("INSERT INTO " . $db_prefix . "setting VALUES 
			(NULL, 0, 'config', 'config_encryption', ?, 0)");

		$product_view = $pdo->prepare("UPDATE " . $db_prefix . "product SET `viewed` = '0'");

		$api_id_delete = $pdo->prepare("DELETE FROM " . $db_prefix . "setting WHERE \"key\"= 'config_api_id'");
		$api_id_insert = $pdo->prepare("INSERT INTO " . $db_prefix . "setting VALUES
			(NULL, 0, 'config', 'config_api_id', ?, 0)");

		$api_insert = $pdo->prepare("INSERT INTO " . $db_prefix . "api VALUES
			(NULL, 'Default', ?, 1, NOW(), NOW())");

		$invoice_update = $pdo->prepare("UPDATE " . $db_prefix . "setting SET 
			value = :value 
			WHERE \"key\" = 'config_invoice_prefix'");

		$user_delete->execute();
		$user_insert->execute([
			'username' => $data['username'],
			'password' => password_hash(html_entity_decode($data['password'], ENT_QUOTES, 'UTF-8'), PASSWORD_DEFAULT),
			'email' => $data['email']
		]);		
		
		$email_delete->execute();
		$email_insert->execute([$data['email']]);

		$product_view->execute();

		$encrypt_delete->execute();
		$encrypt_insert->execute([token(512)]);

		$api_insert->execute([token(256)]);
		$api_id = $pdo->LastInsertId();
		$api_id_delete->execute();
		$api_id_insert->execute([$api_id]);

		$invoice_update->execute(["INV-" . date('Y') . "-00"]);
	}
}
