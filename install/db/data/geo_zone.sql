INSERT INTO `lk_geo_zone` (`geo_zone_id`, `name`, `description`, `date_modified`, `date_added`) VALUES
(1,'Portugal - Continental','Portugal Continental','2022-10-20 19:21:31','2022-10-20 19:21:31'),
(2,'Portugal - Madeira','Região Autónoma da Madeira','2022-10-20 19:29:33','2022-10-20 19:30:59'),
(3,'Portugal - Açores','Arquipélago dos Açores','2022-10-20 19:30:35','2022-10-20 19:30:35'),
(4,'UK VAT Zone','UK VAT','2009-01-06 23:26:25','2010-02-26 22:33:24'),
(5,'UK Shipping','UK Shipping Zones','2009-06-23 01:14:53','2010-12-15 15:18:13');
