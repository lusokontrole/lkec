INSERT INTO `lk_currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(1,'Pound Sterling','GBP','£','','2',0.87728,1,'2022-10-22 15:05:47'),
(2,'US Dollar','USD','$','','2',0.973,1,'2022-10-22 15:05:47'),
(3,'Euro','EUR','','€','2',1,1,'2022-10-22 15:05:47'),
(4,'Hong Kong Dollar','HKD','HK$','','2',7.84885512,0,'2022-10-22 14:50:19'),
(5,'Indian Rupee','INR','₹','','2',82.14200637,0,'2022-10-22 14:50:19'),
(6,'Russian Ruble','RUB','','₽','2',56.40360000,0,'2018-02-22 12:00:00'),
(7,'Chinese Yuan Renminbi','CNY','¥','','2',7.18194887,0,'2022-10-22 14:50:19'),
(8,'Australian Dollar','AUD','$','','2',1.59102577,0,'2022-10-22 14:50:19');
