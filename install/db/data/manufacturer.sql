INSERT INTO `lk_manufacturer` (`manufacturer_id`, `name`, `image`, `sort_order`) VALUES
(1,'Souk','catalog/demo/souk_logo.jpg',0),
(5,'HTC','catalog/demo/htc_logo.jpg',0),
(6,'Palm','catalog/demo/palm_logo.jpg',0),
(7,'Hewlett-Packard','catalog/demo/hp_logo.jpg',0),
(8,'Apple','catalog/demo/apple_logo.jpg',0),
(9,'Canon','catalog/demo/canon_logo.jpg',0),
(10,'Sony','catalog/demo/sony_logo.jpg',0);
