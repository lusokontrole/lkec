<?php
$_['text_subject']		= '%s - Nova palavra-passe';
$_['text_greeting']		= 'Uma nova palavra-passe foi solicitada em: %s.';
$_['text_change']		= 'To reset your password click on the link below:';
$_['text_ip']		= 'The IP used to make this request was:';
