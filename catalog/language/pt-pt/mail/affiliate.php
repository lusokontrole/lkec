<?php
$_['text_subject']		= 'Programa de afiliados %s';
$_['text_welcome']		= 'Obrigado por ter criado uma conta no Programa de afiliados %s!';
$_['text_login']		= 'A conta foi criada no Programa de afiliados, agora pode entrar na sua área de afiliado com o seu e-mail e senha:';
$_['text_approval']		= 'A conta de afiliado precisa de ser aprovada antes de poder entrar na sua área de afiliado. Assim que sua conta for aprovada, será informado por email e pode aceder á sua área de afiliado com o seu e-mail e senha:';
$_['text_service']		= 'Upon logging in, you will be able to generate tracking codes, track commission payments and edit your account information.';
$_['text_thanks']		= 'Atentamente,';
$_['text_new_affiliate']		= 'Novo afiliado';
$_['text_signup']		= 'Dados do novo afiliado:';
$_['text_website']		= 'Website:';
$_['text_customer_group']		= 'Customer Group:';
$_['text_firstname']		= 'Nome:';
$_['text_lastname']		= 'Apelido:';
$_['text_company']		= 'Empresa:';
$_['text_email']		= 'Endereço de e-mail:';
$_['text_telephone']		= 'Telefone:';
