<?php
$_['text_success']		= 'O cliente foi alterado';
$_['error_permission']		= 'Atenção: não tem permissão para aceder a API!';
$_['error_customer']		= 'You must select a customer!';
$_['error_firstname']		= 'O nome deve ter entre 2 e 32 caracteres!';
$_['error_lastname']		= 'O sobrenome deve entre 2 e 32 caracteres!';
$_['error_email']		= 'O endereço de e-mail é inválido!';
$_['error_telephone']		= 'O telefone deve ter no mínimo 9 caracteres!';
$_['error_custom_field']		= 'O campo %s é obrigatório!';
