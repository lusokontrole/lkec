<?php
$_['heading_title']		= 'Pontos de fidelização';
$_['column_date_added']		= 'Registo';
$_['column_description']		= 'Descrição';
$_['column_points']		= 'Pontos';
$_['text_account']		= 'A Minha Conta';
$_['text_reward']		= 'Pontos';
$_['text_total']		= 'Total de pontos:';
$_['text_empty']		= 'Ainda não ganhou nenhuns pontos!';
