<?php
$_['heading_title']		= 'Subcrição da newsletter';
$_['text_account']		= 'A Minha Conta';
$_['text_newsletter']		= 'Subcrição da newsletter';
$_['text_success']		= 'A subcrição da nossa newsletter foi alterada.';
$_['entry_newsletter']		= 'Deseja receber a newsletter?';
