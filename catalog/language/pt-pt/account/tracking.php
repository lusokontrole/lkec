<?php
$_['heading_title']		= 'Tracking Afiliado';
$_['text_account']		= 'Conta';
$_['text_description']		= 'Para garantir que seja pago pelas referências que você nos envia, precisamos fazer tracking da referência, colocando um código de tracking na ligação do URL para nós. Você pode usar as ferramentas abaixo para gerar links para o site %s. ';
$_['entry_code']		= 'O seu Código de Tracking ';
$_['entry_generator']		= 'Tracking Link Generator';
$_['entry_link']		= 'Tracking Link';
$_['help_generator']		= 'Escreva o nome do produto gostaria de linkar';
