<?php
$_['heading_title']		= 'Esqueceu-se da sua palavra-passe?';
$_['text_account']		= 'Criar conta';
$_['text_forgotten']		= 'Esqueceu-se da sua palavra-passe?';
$_['text_your_email']		= 'Endereço e-mail';
$_['text_email']		= 'Insira o endereço e-mail criado na sua conta, e clique no botão <b>Continuar</b>. <br />Será enviado um e-mail com um link para criar uma nova palavra-passe.';
$_['text_success']		= 'Foi enviada uma mensagem para criar uma nova palavra-passe para o seu endereço de e-mail.';
$_['entry_email']		= 'Endereço e-mail';
$_['entry_password']		= 'New Password';
$_['entry_confirm']		= 'Confirm';
$_['error_email']		= 'Atenção: Não foi encontrado nenhum registo do endereço de e-mail inserido. Tente novamente!';
$_['error_approved']		= 'Warning: Your account requires approval before you can login.';
$_['error_password']		= 'Password must be between 4 and 20 characters!';
$_['error_confirm']		= 'Password and password confirmation do not match!';
