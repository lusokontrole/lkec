<?php
$_['heading_title']		= 'Downloads';
$_['text_account']		= 'A Minha Conta';
$_['text_downloads']		= 'Downloads';
$_['text_empty']		= 'Nenhum pedido confirmado tem downloads associados.';
$_['column_order_id']		= 'Pedido Nº';
$_['column_name']		= 'Nome';
$_['column_size']		= 'Tamanho';
$_['column_date_added']		= 'Data';
