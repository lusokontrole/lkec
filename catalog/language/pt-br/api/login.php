<?php
$_['text_success']		= 'A sessão da API foi iniciada com sucesso.';
$_['error_key']		= 'Atenção: A chave de acesso da API não é válida.';
$_['error_ip']		= 'Atenção: O seu IP %s não tem permissão para acessar a API.';
