<?php
$_['text_success']		= 'O cupom foi utilizado com sucesso.';
$_['error_permission']		= 'Atenção: Você não tem permissão para acessar a API.';
$_['error_coupon']		= 'Atenção: O cupom é inválido, expirou, atingiu o seu limite de uso ou já foi utilizado.';
