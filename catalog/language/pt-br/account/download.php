<?php
$_['heading_title']		= 'Downloads';
$_['text_account']		= 'Minha conta';
$_['text_downloads']		= 'Downloads';
$_['text_empty']		= 'Nenhum pedido associado a downloads foi confirmado até o momento.';
$_['column_order_id']		= 'Pedido nº';
$_['column_name']		= 'Nome';
$_['column_size']		= 'Tamanho';
$_['column_date_added']		= 'Data';
