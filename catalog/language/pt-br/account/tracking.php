<?php
$_['heading_title']		= 'Gerador de links';
$_['text_account']		= 'Minha conta';
$_['text_description']		= 'Para ter certeza que suas indicações vão ser registradas corretamente, gere os links para nossos produtos através do formulário abaixo:';
$_['entry_code']		= 'Seu código de afiliação';
$_['entry_generator']		= 'Digite o nome do produto';
$_['entry_link']		= 'Link para divulgação';
$_['help_generator']		= 'Digite o nome de um produto para gerar o link de divulgação.';
