<?php
$_['heading_title']		= 'Minha conta';
$_['text_register']		= 'Registo';
$_['text_login']		= 'Autenticar-se';
$_['text_logout']		= 'Sair';
$_['text_forgotten']		= 'Recuperar senha';
$_['text_account']		= 'Conta de cliente';
$_['text_edit']		= 'Informações da conta';
$_['text_password']		= 'Modificar senha';
$_['text_address']		= 'Endereços';
$_['text_wishlist']		= 'Lista de desejos';
$_['text_order']		= 'Histórico de pedidos';
$_['text_download']		= 'Downloads';
$_['text_reward']		= 'Pontos de fidelização';
$_['text_return']		= 'Devoluções';
$_['text_transaction']		= 'Transações';
$_['text_newsletter']		= 'Newsletter';
$_['text_recurring']		= 'Assinaturas';
