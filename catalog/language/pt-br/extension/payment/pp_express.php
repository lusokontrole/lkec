<?php
$_['express_text_title']		= 'Confirmar pedido';
$_['text_title']		= 'PayPal Express Checkout';
$_['text_cart']		= 'Carrinho de compras';
$_['text_shipping_updated']		= 'Servi�o de envio atualizado';
$_['text_trial']		= '%s a cada %s %s repetido por %s vez(es) ';
$_['text_recurring']		= '%s a cada %s %s';
$_['text_recurring_item']		= 'item assinado';
$_['text_length']		= ' para %s pagamentos';
$_['express_entry_coupon']		= 'C�digo do cupom:';
$_['button_express_coupon']		= 'Add';
$_['button_express_confirm']		= 'Confirm';
$_['button_express_login']		= 'Continue to PayPal';
$_['button_express_shipping']		= 'Update shipping';
$_['error_heading_title']		= 'Ocorreu um erro';
$_['error_too_many_failures']		= 'Seu pagamento falhou por demora na confirma��o';
$_['error_unavailable']		= 'Please use the full checkout with this order';
$_['error_no_shipping']		= 'Warning: No Shipping options are available. Please <a href="%s">contact us</a> for assistance!';
