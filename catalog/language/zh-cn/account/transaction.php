<?php
$_['heading_title']		= '您的交易';
$_['column_date_added']		= '添加日期';
$_['column_description']		= '描述';
$_['column_amount']		= '金额 (%s)';
$_['text_account']		= '帐户';
$_['text_transaction']		= '您的交易';
$_['text_total']		= '您当前的余额为：';
$_['text_empty']		= '您没有任何交易！';
