<?php
$_['heading_title']		= '帐号登录';
$_['text_account']		= '帐户';
$_['text_login']		= '登录';
$_['text_new_customer']		= '新客户';
$_['text_register']		= '注册账户';
$_['text_register_account']		= '通过创建一个帐户，您将能够更快地购物，了解订单状态的最新信息，并跟踪您之前的订单。';
$_['text_returning_customer']		= '回头客';
$_['text_i_am_returning_customer']		= '我是老顾客';
$_['text_forgotten']		= '忘记密码';
$_['entry_email']		= '电子邮件地址';
$_['entry_password']		= '密码';
$_['error_login']		= '警告：电子邮件地址和/或密码不匹配。';
$_['error_attempts']		= '警告：您的帐户已超过允许的登录尝试次数。请在 1 小时后重试。';
$_['error_approved']		= '警告：您的帐户需要批准才能登录。';
