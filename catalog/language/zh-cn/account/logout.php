<?php
$_['heading_title']		= '帐户注销';
$_['text_message']		= '<p>您已注销您的帐户。现在可以安全地离开计算机了。</p><p>您的购物车已保存，只要您重新登录帐户，其中的商品就会恢复。</p>';
$_['text_account']		= '帐户';
$_['text_logout']		= '登出';
