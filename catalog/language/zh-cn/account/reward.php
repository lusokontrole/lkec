<?php
$_['heading_title']		= '您的奖励积分';
$_['column_date_added']		= '添加日期';
$_['column_description']		= '描述';
$_['column_points']		= '积分';
$_['text_account']		= '帐户';
$_['text_reward']		= '奖励分数';
$_['text_total']		= '您的奖励积分总数为：';
$_['text_empty']		= '您没有任何奖励积分！';
