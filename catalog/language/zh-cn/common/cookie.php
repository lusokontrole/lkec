<?php
$_['text_success']		= 'Thank you for letting us know your choice!';
$_['text_cookie']		= 'This website uses cookies. For more information <a href="%s">click here</a>.';
$_['button_agree']		= 'Yes, that\'s fine!';
$_['button_disagree']		= 'No Thanks!';
