<?php
$_['text_subject']		= '%s - 联属计划';
$_['text_welcome']		= '感谢您加入 %s 联盟计划！';
$_['text_login']		= '您的帐户现已创建，您可以通过访问我们的网站或通过以下 URL 使用您的电子邮件地址和密码登录：';
$_['text_approval']		= '必须先批准您的帐户，然后才能登录。一旦获得批准，您可以使用您的电子邮件地址和密码通过访问我们的网站或以下 URL 登录：';
$_['text_service']		= '登录后，您将能够生成跟踪代码、跟踪佣金支付和编辑您的帐户信息。';
$_['text_thanks']		= '谢谢，';
$_['text_new_affiliate']		= '新会员';
$_['text_signup']		= '一个新的附属公司已注册：';
$_['text_website']		= '网站：';
$_['text_customer_group']		= '客户群：';
$_['text_firstname']		= '名：';
$_['text_lastname']		= '姓：';
$_['text_company']		= '公司：';
$_['text_email']		= '电子邮件：';
$_['text_telephone']		= '电话：';
