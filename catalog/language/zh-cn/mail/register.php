<?php
$_['text_subject']		= '%s - 感谢您注册';
$_['text_welcome']		= '欢迎并感谢您在 %s 注册！';
$_['text_login']		= '您的帐户现已创建，您可以通过访问我们的网站或以下 URL 使用您的电子邮件地址和密码登录：';
$_['text_approval']		= '必须先批准您的帐户，然后才能登录。一旦获得批准，您可以使用您的电子邮件地址和密码访问我们的网站或通过以下 URL 登录：';
$_['text_service']		= '登录后，您将能够访问其他服务，包括查看过去的订单、打印发票和编辑您的帐户信息。';
$_['text_thanks']		= '谢谢，';
$_['text_new_customer']		= '新客户';
$_['text_signup']		= '一位新客户已注册：';
$_['text_customer_group']		= '客户群：';
$_['text_firstname']		= '名：';
$_['text_lastname']		= '姓：';
$_['text_email']		= '电子邮件：';
$_['text_telephone']		= '电话：';
