<?php
$_['text_success']		= '成功：您已修改订单！';
$_['error_permission']		= '警告：您没有访问 API 的权限！';
$_['error_customer']		= '警告：需要设置客户详细信息！';
$_['error_payment_address']		= '警告：需要付款地址！';
$_['error_payment_method']		= '警告：需要付款方式！';
$_['error_no_payment']		= '警告：没有可用的付款方式！';
$_['error_shipping_address']		= '警告：需要收货地址！';
$_['error_shipping_method']		= '警告：需要运输方式！';
$_['error_no_shipping']		= '警告：没有可用的运输选项！';
$_['error_stock']		= '警告：标有 *** 的产品没有所需数量或无库存！';
$_['error_minimum']		= '警告：%s 的最低订购量为 %s！';
$_['error_not_found']		= '警告：找不到订单！';
