<?php
$_['text_success']		= '成功：您已经修改了您的购物车！';
$_['error_permission']		= '警告：您没有访问 API 的权限！';
$_['error_stock']		= '标有***的产品没有所需数量或无库存！';
$_['error_minimum']		= '%s 的最低订购量为 %s！';
$_['error_store']		= '产品不能从您选择的商店购买！';
$_['error_required']		= '需要 %s！';
