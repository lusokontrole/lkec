<?php
$_['text_address']		= '成功：收货地址已设置！';
$_['text_method']		= '成功：配送方式已设置！';
$_['error_permission']		= '警告：您没有访问 API 的权限！';
$_['error_firstname']		= '名字必须介于 1 到 32 个字符之间！';
$_['error_lastname']		= '姓氏必须介于 1 到 32 个字符之间！';
$_['error_address_1']		= '地址 1 的长度必须在 3 到 128 个字符之间！';
$_['error_city']		= '城市必须在 3 到 128 个字符之间！';
$_['error_postcode']		= '该国家/地区的邮政编码必须介于 2 到 10 个字符之间！';
$_['error_country']		= '请选择国家！';
$_['error_zone']		= '请选择地区/州！';
$_['error_custom_field']		= '需要 %s！';
$_['error_address']		= '警告：需要收货地址！';
$_['error_method']		= '警告：需要运输方式！';
$_['error_no_shipping']		= '警告：没有可用的运输选项！';
