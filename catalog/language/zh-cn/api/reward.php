<?php
$_['text_success']		= '成功：您的奖励积分折扣已应用！';
$_['error_permission']		= '警告：您没有访问 API 的权限！';
$_['error_reward']		= '警告：请输入要使用的奖励积分数量！';
$_['error_points']		= '警告：您没有 %s 奖励积分！';
$_['error_maximum']		= '警告：可应用的最大点数为 %s！';
