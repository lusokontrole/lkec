<?php
$_['heading_title']		= 'DSGVO Anfrage';
$_['text_account']		= 'Konto';
$_['text_gdpr']		= 'You can view %s GDPR policy on the <a href="%s" target="_blank">%s</a> page.';
$_['text_verification']		= 'Kontoüberprüfung';
$_['text_email']		= 'Vor weiteren Aktionen müssen wir das Konto überprüfen, bitte Emailadresse angeben.';
$_['text_action']		= 'Aktion auswählen';
$_['text_export']		= 'Persönliche Daten exportieren';
$_['text_remove']		= 'Remove Personal Data';
$_['text_warning']		= 'Warning you will lose access to your account!';
$_['text_access']		= 'Es besteht kein Zugriff auf das Konto %s.';
$_['text_history']		= 'Es besteht kein Zugriff auf Auftragsverlauf, Rechnungen, Downloads usw.';
$_['text_limit']		= 'Das Konto wird nach <b>%s Tagen</b> gelöscht damit eventuell noch ausstehende Tranaktionen ausgeführt werden können.';
$_['text_success']		= 'DSGVO Anfrage erfolgreich eingegangen, Email wurde zugesendet.';
$_['entry_email']		= 'Email';
$_['error_email']		= 'Emailadresse ist nicht gültig';
$_['error_action']		= 'Bitte eine gültige Auswahl vornehmen';
