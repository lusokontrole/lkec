<?php
$_['heading_title']		= 'Kontakt';
$_['text_location']		= 'Unsere Adresse';
$_['text_store']		= 'Unsere Shops';
$_['text_contact']		= 'Kontaktformular';
$_['text_address']		= 'Adresse';
$_['text_telephone']		= 'Telefon';
$_['text_open']		= 'Öffnungszeiten';
$_['text_comment']		= 'Anmerkung';
$_['text_message']		= '<p>Nachricht wurde erfolgreich versendet</p>';
$_['entry_name']		= 'Name';
$_['entry_email']		= 'Emailadresse';
$_['entry_enquiry']		= 'Anfrage / Text';
$_['email_subject']		= 'Nachricht %s';
$_['error_name']		= 'Name muss zwischen 3 und 32 Zeichen lang sein';
$_['error_email']		= 'Emailadresse ist nicht gültig';
$_['error_enquiry']		= 'Text muss zwischen 10 und 3000 Zeichen lang sein';
