<?php
$_['heading_title']		= 'Newsletter';
$_['text_account']		= 'Konto';
$_['text_newsletter']		= 'Newsletter';
$_['text_success']		= 'An-/bzw. Abmeldung zum Newsletter erfolgreich übermittelt';
$_['entry_newsletter']		= 'Abonniert<p style="color: #676767; font-size: 0.9em;"><em>(Zum An- bzw. Abmelden gewünschten Status auswählen und auf Weiter klicken)</em></p>';
