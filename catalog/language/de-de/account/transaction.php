<?php
$_['heading_title']		= 'Meine Transaktionen';
$_['column_date_added']		= 'Datum';
$_['column_description']		= 'Beschreibung';
$_['column_amount']		= 'Betrag (%s)';
$_['text_account']		= 'Konto';
$_['text_transaction']		= 'Transaktionen';
$_['text_total']		= 'Aktueller Saldo';
$_['text_empty']		= 'Noch keine Transaktionen vorhanden';
