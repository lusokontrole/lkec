<?php
$_['heading_title']		= 'Meine Wunschliste';
$_['text_account']		= 'Konto';
$_['text_instock']		= 'Lagernd';
$_['text_wishlist_total']		= 'Wish List (%s)';
$_['text_login']		= 'Bitte entweder <a href="%s" title="Anmeldung">anmelden</a> oder <a href="%s" title="Registrierung">registrieren</a> um <a href="%s">%s</a> die <a href="%s" title="Wunschliste">Wunschliste</a> zu speichern';
$_['text_success']		= '<a href="%s">%s</a> wurde erfolgreich der <a href="%s">Wunschliste</a> hinzugefügt';
$_['text_remove']		= 'Wunschliste wurde erfolgreich bearbeitet';
$_['text_empty']		= 'Die Wunschliste ist noch leer';
$_['column_image']		= 'Bild';
$_['column_name']		= 'Name';
$_['column_model']		= 'Artikelnr.';
$_['column_stock']		= 'Lager';
$_['column_price']		= 'Einzelpreis';
$_['column_action']		= 'Aktion';
