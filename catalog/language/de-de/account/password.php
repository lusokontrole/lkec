<?php
$_['heading_title']		= 'Passwort ändern';
$_['text_account']		= 'Konto';
$_['text_password']		= 'Passwort';
$_['text_success']		= 'Passwort wurde erfolgreich geändert';
$_['entry_password']		= 'Passwort';
$_['entry_confirm']		= 'Wiederholung';
$_['error_password']		= 'Passwort muss zwischen 4 und 20 Zeichen lang sein';
$_['error_confirm']		= 'Passwort und Wiederholung stimmen nicht überein';
