<?php
$_['text_subject']		= '%s - DSGVO Anfrage zu Kontolöschung';
$_['text_request']		= 'Account Deletion Request';
$_['text_hello']		= 'Guten Tag <strong>%s</strong>';
$_['text_user']		= 'User';
$_['text_delete']		= 'Das Konto wird nach <strong>%s Tagen</strong> gelöscht damit eventuell noch offene Transaktionen durchgeführt werden können.';
$_['text_contact']		= 'Falls diese Anfrage nicht für diese Konto gestellt wurde, bitte den Inhaber informieren:';
$_['text_thanks']		= 'Danke,';
$_['button_contact']		= 'Uns kontaktieren';
