<?php
$_['text_success']		= 'Währung erfolgreich geändert';
$_['error_permission']		= 'Keine Rechte die API anzuwenden oder die Session ist abgelaufen (bitte neu anmelden oder Seite neu laden)';
$_['error_currency']		= 'Währungscode ist nicht gültig';
