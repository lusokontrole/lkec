<?php
$_['text_success']		= 'Gutschein erfolgreich angewendet';
$_['error_permission']		= 'Keine Rechte die API anzuwenden oder die Session ist abgelaufen (bitte neu anmelden oder Seite neu laden)';
$_['error_coupon']		= 'Hinweis: der Gutschein ist entweder ungültig, abgelaufen oder hat das maximale Einlösungslimit erreicht';
