<?php
$_['text_upload']		= 'Datei erfolgreich hochgeladen';
$_['error_filename']		= 'Dateiname muss zwischen 3 und 64 Zeichen lang sein';
$_['error_filetype']		= 'Ungültige Dateiart';
$_['error_upload']		= 'Upload erforderlich';
