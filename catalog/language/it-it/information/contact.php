<?php
$_['heading_title']		= 'Contattaci';
$_['text_location']		= 'Dove Siamo';
$_['text_store']		= 'I nostri negozi';
$_['text_contact']		= 'Modulo di Contatto';
$_['text_address']		= 'Indirizzi:';
$_['text_telephone']		= 'Telefono:';
$_['text_open']		= 'orari di Apertura';
$_['text_comment']		= 'Commenti';
$_['text_message']		= '<p>La tua richiesta &egrave; stata correttamente inviata al titolare del negozio!</p>';
$_['entry_name']		= 'Nome:';
$_['entry_email']		= 'Indirizzo E-Mail:';
$_['entry_enquiry']		= 'Richiesta:';
$_['email_subject']		= 'Richiesta %s';
$_['error_name']		= 'Il Nome deve essere tra 3 e 32 caratteri!';
$_['error_email']		= 'L\'indirizzo E-Mail non sembra valido!';
$_['error_enquiry']		= 'La richiesta deve essere lunga dai 10 ai 3000 caratteri!';
