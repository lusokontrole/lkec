<?php
$_['text_wishlist']		= 'Lista dei Desideri (%s)';
$_['text_shopping_cart']		= 'Carrello';
$_['text_account']		= 'Account';
$_['text_register']		= 'Registrazione';
$_['text_login']		= 'Accesso';
$_['text_order']		= 'Cronologia ordini';
$_['text_transaction']		= 'Transazioni';
$_['text_download']		= 'Download';
$_['text_logout']		= 'Esci';
