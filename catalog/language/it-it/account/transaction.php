<?php
$_['heading_title']		= 'Le tue Transazioni';
$_['column_date_added']		= 'Data Inserimento';
$_['column_description']		= 'Descrizione';
$_['column_amount']		= 'Importo; (%s)';
$_['text_account']		= 'Account';
$_['text_transaction']		= 'Le tue Transazioni';
$_['text_total']		= 'Il tuo attuale bilancio &egrave;:';
$_['text_empty']		= 'Non hai effettuato alcuna transazione!';
