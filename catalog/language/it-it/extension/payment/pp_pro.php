<?php
$_['text_title']		= 'Carta di Credito o di Debito (Elaborati in modo sicuro tramite PayPal)';
$_['text_wait']		= 'Si prega di attendere!';
$_['text_credit_card']		= 'Dettagli Carta di Credito';
$_['entry_cc_type']		= 'Tipo di Carta:';
$_['entry_cc_number']		= 'Numero di Carta:';
$_['entry_cc_start_date']		= 'Carta Valida da:';
$_['entry_cc_expire_date']		= 'Data Scadenza Carta:';
$_['entry_cc_cvv2']		= 'Codice di Sicurezza della Carta (CVV2):';
$_['entry_cc_issue']		= 'Numero di rilascio Carta:';
$_['help_start_date']		= '(if available)';
$_['help_issue']		= '(for Maestro and Solo cards only)';
