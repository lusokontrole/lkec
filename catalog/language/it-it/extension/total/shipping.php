<?php
$_['heading_title']		= 'Costi di Spedizione e Tasse stimati';
$_['text_success']		= 'La stima del costo della tua spedizione &egrave; stata effettuata!';
$_['text_shipping']		= 'Inserisci la destinazione per avere una stima del costo di spedizione.';
$_['text_shipping_method']		= 'Please select the preferred shipping method to use on this order.';
$_['entry_country']		= 'Paese:';
$_['entry_zone']		= 'Regione / Stato:';
$_['entry_postcode']		= 'CAP:';
$_['error_postcode']		= 'Il CAP deve essere compreso tra 2 e 10 caratteri!';
$_['error_country']		= 'Si Prega di selezionare un paese!';
$_['error_zone']		= 'Si Prega di selezionare una regione / stato!';
$_['error_shipping']		= 'Errore: Metodo di spedizione richiesto!';
$_['error_no_shipping']		= 'Errore: Nessuna opzione di spedizione disponibile. Per favore, <a href="%s">contattaci</a> per assistenza!';
