<?php
$_['heading_title']		= 'Codice Sconto';
$_['text_coupon']		= 'Coupon (%s):';
$_['text_success']		= 'Successo: Il tuo coupon &egrave; stato applicato!';
$_['entry_coupon']		= 'Inserisci il tuo Coupon qui:';
$_['error_coupon']		= 'Errore: Il Coupon non &egrave; valido, &egrave; scaduto o ha raggiunto il limite di utilizzo!';
$_['error_empty']		= 'Warning: Please enter a coupon code!';
