<?php
$_['heading_title']		= 'Account';
$_['text_register']		= 'Registrati';
$_['text_login']		= 'Login';
$_['text_logout']		= 'Logout';
$_['text_forgotten']		= 'Password Dimenticata';
$_['text_account']		= 'Il Mio Account';
$_['text_edit']		= 'Modifica Account';
$_['text_password']		= 'Password';
$_['text_address']		= 'Address Book';
$_['text_wishlist']		= 'Lista dei Desideri';
$_['text_order']		= 'Storico Ordini';
$_['text_download']		= 'Downloads';
$_['text_reward']		= 'Reward Points';
$_['text_return']		= 'Restituzioni';
$_['text_transaction']		= 'Transazioni';
$_['text_newsletter']		= 'Newsletter';
$_['text_recurring']		= 'Recurring payments';
