<?php
$_['text_success']		= 'Sessione API correttamente avviata!';
$_['error_key']		= 'Attenzione: Chiave API errata!';
$_['error_ip']		= 'Attenzione: Il tuo IP %s non &egrave; autorizzato ad accedere alle API!';
