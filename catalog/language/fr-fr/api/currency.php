<?php
$_['text_success']		= 'Succès : vous avez correctement modifié la devise !';
$_['error_permission']		= 'Attention : vous n’avez pas la permission d’accéder à l’API !';
$_['error_currency']		= 'Attention : le code de la devise est invalide !';
