<?php
$_['text_success']		= 'Le profil a été modifié avec succès';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de accéder à l’API !';
$_['error_customer']		= 'You must select a customer!';
$_['error_firstname']		= 'Le prénom doit contenir entre 1 à 32 caractères !';
$_['error_lastname']		= 'Le nom de famille doit contenir entre 1 à 32 caractères !';
$_['error_email']		= 'L’adresse électronique ne semble pas être valide !';
$_['error_telephone']		= 'Le numéro de téléphone doit contenir entre 3 et 32 caractères !';
$_['error_custom_field']		= '%s requis !';
