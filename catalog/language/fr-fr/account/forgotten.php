<?php
$_['heading_title']		= 'Avez-vous oublié votre mot de passe ?';
$_['text_account']		= 'Compte';
$_['text_forgotten']		= 'Mot de passe oublié';
$_['text_your_email']		= 'Votre adresse électronique';
$_['text_email']		= 'Saisissez l’adresse électronique associée à votre compte. Cliquez sur soumettre pour obtenir un lien de réinitialisation par courrier électronique.';
$_['text_success']		= 'Un courrier électronique contenant un lien de confirmation vient d’être envoyé à votre adresse.';
$_['entry_email']		= 'Adresse électronique';
$_['entry_password']		= 'Nouveau mot de passe';
$_['entry_confirm']		= 'Confirmer';
$_['error_email']		= 'Attention : cette adresse électronique n’a pas été trouvée dans nos fichiers, veuillez essayer à nouveau !';
$_['error_approved']		= 'Attention : votre compte nécessite d’avoir été approuvé avant que vous puissiez vous connecter.';
$_['error_password']		= 'Le mot de passe doit contenir entre 4 et 20 caractères !';
$_['error_confirm']		= 'Le mot de passe de confirmation ne correspond pas au premier mot de passe !';
