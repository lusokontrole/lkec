<?php
$_['heading_title']		= 'Réinitialiser votre mot de passe';
$_['text_account']		= 'Compte';
$_['text_password']		= 'Saisissez le nouveau mot de passe que vous souhaitez utiliser.';
$_['text_success']		= 'Succès : votre mot de passe a bien été modifié.';
$_['entry_password']		= 'Mot de passe';
$_['entry_confirm']		= 'Confirmer';
$_['error_password']		= 'Le mot de passe doit contenir entre 4 et 20 caractères !';
$_['error_confirm']		= 'Le mot de passe de confirmation ne correspond pas au premier mot de passe !';
$_['error_code']		= 'Le code de réinitialisation du mot de passe est invalide ou a déjà été utilisé précédemment !';
