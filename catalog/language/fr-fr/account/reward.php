<?php
$_['heading_title']		= 'Vos points de fidélité';
$_['column_date_added']		= 'Date d’ajout';
$_['column_description']		= 'Description';
$_['column_points']		= 'Points';
$_['text_account']		= 'Compte';
$_['text_reward']		= 'Points de fidélité';
$_['text_total']		= 'Votre total de points de fidélité est de : ';
$_['text_empty']		= 'Vous n’avez aucun point de fidélité !';
