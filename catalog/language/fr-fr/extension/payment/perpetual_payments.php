<?php
$_['text_title']		= 'Carte de Cr&eacute;dit ou de D&eacute;bit (Traitement s&eacute;curis&eacute; par Perpetual Payments)';
$_['text_credit_card']		= 'D&eacute;tails carte de cr&eacute;dit';
$_['text_transaction']		= 'Num&eacute;ro de transaction :';
$_['text_avs']		= 'AVS/CVV :';
$_['text_avs_full_match']		= 'Correspondant';
$_['text_avs_not_match']		= 'Ne correspondant pas';
$_['text_authorisation']		= 'Code d&#8217;autorisation:';
$_['entry_cc_number']		= 'Num&eacute;ro carte :';
$_['entry_cc_start_date']		= 'Carte valide depuis le :';
$_['entry_cc_expire_date']		= 'Date expiration carte :';
$_['entry_cc_cvv2']		= 'Code s&eacute;curit&eacute; carte (CVV2) :';
$_['entry_cc_issue']		= 'Num&eacute;ro d&#8217;&eacute;mission carte :';
$_['help_start_date']		= '(if available)';
$_['help_issue']		= '(for Maestro and Solo cards only)';
