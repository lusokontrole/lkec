<?php
$_['heading_title']		= 'Estimation de livraison et taxes';
$_['text_success']		= 'F&eacute;licitations, le montant estimatif de livraison a &eacute;t&eacute; appliqu&eacute; avec succ&egrave;s !';
$_['text_shipping']		= 'Entrez votre destination pour obtenir un montant estimatif de livraison.';
$_['text_shipping_method']		= 'Please select the preferred shipping method to use on this order.';
$_['entry_country']		= 'Pays :';
$_['entry_zone']		= 'D&eacute;partement :';
$_['entry_postcode']		= 'Code postal :';
$_['error_postcode']		= 'Le code postal doit &ecirc;tre compos&eacute; de 2 &agrave; 10 caract&egrave;res ! ';
$_['error_country']		= 'Veuillez s&eacute;lectionner un pays !';
$_['error_zone']		= 'Veuillez s&eacute;lectionner un d&eacute;partement dans la liste !';
$_['error_shipping']		= 'Erreur : Vous devez s&eacute;lectionner un mode de livraison !';
$_['error_no_shipping']		= 'Erreur : Aucune option de livraison n&#8217;est disponible.<br>Veuillez <a href="%s">nous contacter</a> pour obtenir de l&#8217;assistance !';
