<?php
$_['heading_title']		= 'Contactez-nous';
$_['text_location']		= 'Adresse du magasin';
$_['text_store']		= 'Boutiques';
$_['text_contact']		= 'Formulaire de contact';
$_['text_address']		= 'Adresse';
$_['text_telephone']		= 'Téléphone';
$_['text_open']		= 'Horaires d’ouverture';
$_['text_comment']		= 'Commentaires';
$_['text_message']		= '<p>Votre demande de renseignements nous a &eacute;t&eacute; envoy&eacute;e avec succ&egrave;s !</p>';
$_['entry_name']		= 'Votre nom';
$_['entry_email']		= 'Adresse électronique';
$_['entry_enquiry']		= 'Demande de renseignements';
$_['email_subject']		= 'Demande de renseignements : %s';
$_['error_name']		= 'Attention : le nom doit contenir entre 3 et 32 caractères !';
$_['error_email']		= 'Attention : l’adresse électronique ne semble pas valide !';
$_['error_enquiry']		= 'Attention : la demande de renseignements doit contenir entre 10 et 3000 caractères !';
