<?php
$_['text_upload']		= 'Transfert du fichier réussi !';
$_['error_filename']		= 'Attention : le nom du fichier doit contenir entre 3 et 128 caractères !';
$_['error_filetype']		= 'Attention : type de fichier incorrect !';
$_['error_upload']		= 'Attention : transfert de fichier nécessaire !';
