<?php
$_['text_information']		= 'Información';
$_['text_service']		= 'Servicio al Cliente';
$_['text_extra']		= 'Extras';
$_['text_contact']		= 'Contáctenos';
$_['text_return']		= 'Devoluciones';
$_['text_sitemap']		= 'Mapa del Sitio';
$_['text_gdpr']		= 'GDPR';
$_['text_manufacturer']		= 'Marcas';
$_['text_voucher']		= 'Tarjetas de Regalo';
$_['text_affiliate']		= 'Afiliado';
$_['text_special']		= 'Especiales';
$_['text_account']		= 'Mi cuenta';
$_['text_order']		= 'Historial de Pedidos';
$_['text_wishlist']		= 'Lista de Deseos';
$_['text_newsletter']		= 'Boletín de Notícias';
$_['text_powered'] = 'Powered By <a href="https://khadija.agency">Souk</a><br /> %s &copy; %s';
