<?php
$_['text_wishlist']		= 'Listas de Deseos';
$_['text_shopping_cart']		= 'Carrito de Compras';
$_['text_account']		= 'Mi Cuenta';
$_['text_register']		= 'Registro';
$_['text_login']		= 'Iniciar Sesión';
$_['text_order']		= 'Historial de Pedidos';
$_['text_transaction']		= 'Transacción';
$_['text_download']		= 'Descargas';
$_['text_logout']		= 'Cerrar Sesión';
