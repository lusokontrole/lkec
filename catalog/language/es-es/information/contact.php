<?php
$_['heading_title']		= 'Contáctenos';
$_['text_location']		= 'Nuestra Ubicación';
$_['text_store']		= 'Nuestras Tiendas';
$_['text_contact']		= 'Formulario de Contacto';
$_['text_address']		= 'Dirección';
$_['text_telephone']		= 'Teléfono';
$_['text_open']		= 'Horarios';
$_['text_comment']		= 'Comentarios';
$_['text_message']		= '<p>La vostra consulta ha estat enviada amb &#200; al propietari de la botiga!</p>';
$_['entry_name']		= 'Nombre';
$_['entry_email']		= 'Correo Electrónico';
$_['entry_enquiry']		= 'Investigación';
$_['email_subject']		= 'Consulta %s';
$_['error_name']		= '¡El nombre debe tener entre 3 y 32 caracteres!';
$_['error_email']		= '¡La dirección de correo electrónico no parece válida!';
$_['error_enquiry']		= '¡La investigación debe tener entre 10 y 3000 caracteres!';
