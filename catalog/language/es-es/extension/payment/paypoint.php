<?php
$_['heading_title']		= 'Gr&#224;cies per comprar amb %s .... ';
$_['text_title']		= 'Targeta de Cr&#232;dit / Targeta de D&#232;bit (PayPoint)';
$_['text_response']		= 'Resposta de PayPoint:';
$_['text_success']		= '... els vostre pagament ha estat rebut amb &#200;.';
$_['text_success_wait']		= '<b><span style="color: #FF0000">Si us plau, espereu...</span></b> mentre acabem de processar la vostre comanda.<br>Si no sou autom&#224;ticament redirigits en 10 segons, si us plau, feu <a href="%s">click aqu&#237;</a>.';
$_['text_failure']		= '... El vostre pagament ha estat Your payment has been cancel&#183;lat!';
$_['text_failure_wait']		= '<b><span style="color: #FF0000">Si us plau, espereu...</span></b><br>Si no sou autom&#224;ticament redirigits en 10 segons, si us plau, feu click<a href="%s">aqu&#237;</a>.';
