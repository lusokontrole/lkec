<?php
$_['text_title']		= 'Targeta de Cr&#232;dit / Targeta de D&#232;bit (Programari Web de Pagament)';
$_['text_credit_card']		= 'Detalls de Targeta de Cr&#232;dit';
$_['entry_cc_owner']		= 'Propietari de Targeta:';
$_['entry_cc_number']		= 'N&#250;mero de Targeta:';
$_['entry_cc_expire_date']		= 'Data de Expiraci&#243;:';
$_['entry_cc_cvv2']		= 'Codi de Seguretat (CVV2):';
