<?php
$_['text_title']		= 'Targeta de Cr&#232;dit / Targeta de D&#232;bit (Paymate)';
$_['text_unable']		= 'No podem localitar o actualitzar l&#039;estat de la vostra comanda';
$_['text_declined']		= 'El Pagament ha estat declinat per Paymate';
$_['text_failed']		= 'Transacci&#243; Paymate Fracassada';
$_['text_failed_message']		= '<p>Desafortunadament es va produir un error en processar la vostre transacci&#243; Paymate.</p><p><b>Advert&#232;ncia: </b>%s</p><p>Si us plau, verifiqueu el vostre balan&#231; al compte Paymate abans d&#039;intentar re-processar aquesta ordre</p><p> Si creieu qeu aquesta transacci&#243; ha estat completada amb &#200;. o mostra una deducci&#243; en el vostre compte Paymate, si us plau, <a href="%s">Contacteu-nos</a> amb els detalls de la comanda.</p>';
$_['text_basket']		= 'Cistella';
$_['text_checkout']		= 'Caixa';
$_['text_success']		= '&#200;xit';
