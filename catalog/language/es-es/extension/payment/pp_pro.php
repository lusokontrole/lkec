<?php
$_['text_title']		= 'Targeta de Cr&#232;dit o D&#232;bit (Processat de forma segura per PayPal)';
$_['text_wait']		= 'Si us plau, espereu!';
$_['text_credit_card']		= 'Detalls Targeta de Cr&#232;dit';
$_['entry_cc_type']		= 'Tipus de Targeta:';
$_['entry_cc_number']		= 'No. de Targeta:';
$_['entry_cc_start_date']		= 'V&#224;lida des de:';
$_['entry_cc_expire_date']		= 'Data de Caducitat:';
$_['entry_cc_cvv2']		= 'Codi de Seguretat de la Targete (CVV2):';
$_['entry_cc_issue']		= 'No. d&#039;Edici&#243; de la Targeta (Card Issue Number):';
$_['help_start_date']		= '(if available)';
$_['help_issue']		= '(for Maestro and Solo cards only)';
