<?php
$_['text_title']		= 'Targeta de Cr&#232;dit / Targeta de D&#232;bit (SagePay)';
$_['text_credit_card']		= 'Detalla de la Targeta de Cr&#232;dit';
$_['entry_cc_owner']		= 'Titular de la Targeta:';
$_['entry_cc_number']		= 'No. de Targeta:';
$_['entry_cc_expire_date']		= 'Data de Caducitat:';
$_['entry_cc_cvv2']		= 'Codi de Seguretat de la Targeta (CVV2):';
