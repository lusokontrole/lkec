<?php
$_['heading_title']		= 'Compte';
$_['text_register']		= 'Registre';
$_['text_login']		= 'Login (Entrar)';
$_['text_logout']		= 'Logout (Sortir)';
$_['text_forgotten']		= 'Contrasenya Perduda';
$_['text_account']		= 'El Meu Compte';
$_['text_edit']		= 'Editar Compte';
$_['text_password']		= 'Contrasenya';
$_['text_address']		= 'Address Book';
$_['text_wishlist']		= 'Llista de Favorits';
$_['text_order']		= 'Historial de Comandes';
$_['text_download']		= 'Desc&#224;rregues';
$_['text_reward']		= 'Reward Points';
$_['text_return']		= 'Devolucions';
$_['text_transaction']		= 'Transaccions';
$_['text_newsletter']		= 'Butllet&#237; de Not&#237;cies';
$_['text_recurring']		= 'Recurring payments';
