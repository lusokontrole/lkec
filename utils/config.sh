#!/bin/sh

DB_PASS=database_password
DB_USER=database_user
DB_NAME=database_name
DB_PREFIX=database_prefix

USER_NAME=admin_login
PASSWORD=admin_password
EMAIL=contact@example.com

ROOT_PATH=/home/user/www
MYSQL_OUTPUT=/home/user/sql/$(date +%Y%j%H%S).sql
RELEASES_PATH=/home/user/releases

VERSION=$(grep VERSION $ROOT_PATH/index.php)
VERSION=$(echo $VERSION | cut -d "'" -f 4)
