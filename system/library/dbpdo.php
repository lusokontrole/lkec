<?php
/**
 * @package		Souk
 * @author		Silvino Silva
 * @copyright	Copyright (c) 2022 - 2022, Souk, Ltd. (https://www.khadija.agency/)
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://www.khadija.agency
*/

/**
* DB
*/
class DBPDO extends PDO {

	/**
	 * Constructor
	 *
	 * @param	string	$adaptor
	 * @param	string	$hostname
	 * @param	string	$username
     * @param	string	$password
	 * @param	string	$database
	 * @param	int		$port
	 *
 	*/
	public function __construct(string $adaptor, string $hostname, string $username, string $password, string $database, string $port = '') {
		if($adaptor == "mysqli") {
			$adaptor = "mysql";
		}

		$dsn = $adaptor.":host=".$hostname.";";
		$dsn = $dsn . "dbname=".$database.";";
		$dsn = $dsn . "port=".$port.";charset=utf8mb4";

		$options = [
		    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
		    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
		    PDO::ATTR_EMULATE_PREPARES   => false,
		];

		return parent::__construct($dsn, $username, $password, $options);
	}
}
