<?php
/**
 * @package		Souk
 * @author		Silvino Silva
 * @copyright	Copyright (c) 2005 - 2017, Souk, Ltd. (https://khadija.agency/)
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://khadija.agency
*/

/**
* Log class
*/
class Log {
	private $file;
	private $message = '';

	/**
	 * Constructor
	 *
	 * @param	string	$filename
 	*/
	public function __construct($filename) {
		$this->file = DIR_LOGS . $filename;
	}
	
	/**
     * 
     *
     * @param	string	$message
     */
	public function write($message) {
		$this->message .= date('Y-m-d H:i:s') . ' - ' . print_r($message, true) . "\n";
	}
	
	/**
     * 
     *
     */
	public function __destruct() {
		file_put_contents($this->file, $this->message, FILE_APPEND);
	}
}