<?php
/**
 * @package		Souk
 * @author		Silvino Silva
 * @copyright	Copyright (c) 2005 - 2017, Souk, Ltd. (https://khadija.agency/)
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://khadija.agency
*/

/**
* Model class
*/
abstract class Model {
	protected $registry;

	public function __construct($registry) {
		$this->registry = $registry;
	}

	public function __get($key) {
		return $this->registry->get($key);
	}

	public function __set($key, $value) {
		$this->registry->set($key, $value);
	}
}